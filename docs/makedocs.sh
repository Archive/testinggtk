#!/bin/sh

EPYOPTS="--no-private -v --config epydoc.config"
rm -rf html
epydoc $EPYOPTS ../tests --html
