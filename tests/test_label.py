'''
Tests for the ``gtk.Label`` class.
'''
import gtk
from gtk import gdk
import math
import pango
import utils

def realize(widget, x, y, width, height):
    event_mask = (widget.get_events()
                  | gdk.EXPOSURE_MASK
                  | gdk.BUTTON_MOTION_MASK
                  | gdk.BUTTON_PRESS_MASK
                  | gdk.BUTTON_RELEASE_MASK
                  | gdk.POINTER_MOTION_MASK)
    widget.window = gdk.Window(None,
                               width, height,
                               gdk.WINDOW_CHILD,
                               event_mask,
                               gdk.INPUT_OUTPUT,
                               x = x, y = y)
    widget.set_style(widget.style.attach(widget.window))

def materialize(widget, x, y, width, height):
    realize(widget, x, y, width, height)
    widget.size_allocate(gdk.Rectangle(x, y, width, height))

def test_default_attributes():
    label = gtk.Label()
    assert not label.get_angle()
    assert not label.get_attributes()
    assert not label.get_ellipsize()
    assert label.get_justify() == gtk.JUSTIFY_LEFT
    assert not label.get_label()
    assert label.get_max_width_chars() == -1
    assert label.get_mnemonic_keyval() == gtk.keysyms.VoidSymbol
    assert not label.get_mnemonic_widget()
    assert not label.get_selectable()
    assert not label.get_selection_bounds()
    assert not label.get_single_line_mode()
    assert not label.get_use_markup()
    assert not label.get_use_underline()
    assert label.get_width_chars() == -1
    assert not label.get_line_wrap()
    assert label.get_layout()

def test_label_get_label():
    '''
    Ensure that get_label() returns the unparsed label text.
    '''
    label = gtk.Label('<b>HI</b>')
    label.set_use_markup(True)
    assert label.get_label() == '<b>HI</b>'

@utils.fail_on_warnings    
def test_unrealized_layout_offsets():
    '''
    Ensure that by default, the layout offsets for a ``gtk.Label`` is
    ``(-1,-1)``.

    :bug: #530255
    '''
    label = gtk.Label('<b>Hi</b>')
    assert label.get_layout_offsets() == (-1, -1)
    label.set_ellipsize(True)
    assert label.get_layout_offsets() == (-1, -1)

def test_toplevel_widget():
    '''
    Ensure that the topmost widget for a newly constructed label is
    the label itself.
    '''
    label = gtk.Label('hi')
    assert label.get_toplevel() == label

def test_parent_window():
    '''
    Ensure that a newly constructed label's parent does not have a
    window.
    '''
    label = gtk.Label('hi')
    assert not label.get_parent_window()

def test_same_window_as_parent():
    '''
    A realized ``gtk.Label`` shares the same ``gdk.Window`` as its
    parent.
    '''
    label = gtk.Label('hi')
    win = gtk.Window()
    win.add(label)
    win.show_all()
    assert label.window and win.window
    assert label.window is win.window

def test_unrealized_get_layout():
    label = gtk.Label('<b>Hi</b>')
    layout = label.get_layout()
    width, height = layout.get_pixel_size()
    assert width > 0 and height > 0

def test_bold_wider_than_normal():
    label1 = gtk.Label('<b>M</b>')
    bold_width, bold_height = label1.get_layout().get_pixel_size()
    label2 = gtk.Label('M')
    width, height = label2.get_layout().get_pixel_size()

    assert bold_width > width
    assert bold_height >= height

def test_realized_layout_offsets():
    button = gtk.Button('boo')
    materialize(button, 0, 0, 160, 20)

    alloc = button.allocation

    label = button.get_child()

    xpad, ypad = label.get_padding()
    xalign, yalign = label.get_alignment()
    
    xofs, yofs = label.get_layout_offsets()
    assert xofs == alloc.x + xpad + xalign * alloc.width
    assert yofs == alloc.y + ypad + yalign * alloc.height

    assert xofs == 80
    assert yofs == 10

def test_realized_layout_offsets_in_small_widget():
    button = gtk.Button('HELLO')
    label = button.get_child()
    for x in range(10, 40):
        button.set_size_request(50, x)
        materialize(button, 0, 0, 50, x)

        ypad = label.get_padding()[1]
        yalign = label.get_alignment()[1]
        alloc = label.allocation
        req_height = label.size_request()[1]

        expected_y = int(math.floor(alloc.y + int(ypad) + max((alloc.height - req_height) * yalign, 0)))
        yofs = label.get_layout_offsets()[1]

        assert yofs == expected_y

def test_label_attribute():
    label = gtk.Label()
    assert label.get_label() == ''

    
