'''
Tests for the ``gtk.ListStore`` class.
'''
import gtk
import utils

def test_iter_in_empty_store():
    liststore = gtk.ListStore(str)
    first_iter = liststore.get_iter_first()
    assert not first_iter

def test_get_path():
    liststore = gtk.ListStore(str)

    for x in range(10):
        iter = liststore.append(['hi'])
        path = liststore.get_path(iter)
        assert path == (x,)

def test_get_iter_from_path():
    store = gtk.ListStore(str)
    store.append(['hi'])
    iter = store.get_iter((0,))
    assert store.get_value(iter, 0) == 'hi'

def test_row_changed_signal():
    '''
    Ensure that the ``row-changed`` signal is emitted when a row is
    added to a ``gtk.TreeModel``.
    '''
    emitted = [False]
    def row_changed_cb(model, path, iter):
        emitted[0] = True
    liststore = gtk.ListStore(str)
    liststore.connect('row-changed', row_changed_cb)
    liststore.append(['foo'])
    assert emitted[0]

def test_adding_invalid_row():
    '''
    Ensure that a ``TypeError`` is raised when a row of the wrong type
    is added to a ``gtk.ListStore``.
    '''
    store = gtk.ListStore(int)
    try:
        store.append(['foo'])
        assert False
    except TypeError:
        assert True

def test_adding_wrong_length_row():
    '''
    Ensure that a ``ValueError`` is raised if a row that has wrong
    length is added to a ``gtk.ListStore``.
    '''
    store = gtk.ListStore(int, int)
    try:
        store.append(1, 2, 3, 4)
        assert False
    except TypeError:
        assert True
    try:
        store.append(1)
        assert False
    except TypeError:
        assert True

@utils.pass_on_warnings        
def test_set_column_types_on_non_empty_store():
    '''
    Ensure that a warning is printed if ``set_column_types`` is called
    on a ``gtk.ListStore`` with one row.
    '''
    store = gtk.ListStore(str, int, bool)
    store.append(['hi', 3, False])
    store.set_column_types(str)

def test_set_colum_types():
    '''
    Ensure that increasing the number of columns in ``gtk.ListStore``
    is possible using ``set_column_types``.
    '''
    store = gtk.ListStore(int)
    for x in range(1, 50):
        cols = [int] * x
        store.set_column_types(*cols)
    
def test_clear():
    '''
    Ensure that ``clear`` removes all items in ``gtk.ListStore``.
    '''
    store = gtk.ListStore(str)
    for x in range(10):
        store.append(['hi'])
    assert len(store) == 10
    store.clear()
    assert len(store) == 0

def test_get_value_oob():
    '''
    Ensure that a ``ValueError`` is raised when ``get_value`` is called
    with a column index out of range.
    '''
    store = gtk.ListStore(int)
    store.append([1])
    iter = store.get_iter_first()
    try:
        store.get_value(iter, 1)
        assert False
    except ValueError:
        assert True
    try:
        store.get_value(iter, -1)
        assert False
    except ValueError:
        assert True

def test_append_and_del_items():
    '''
    Ensure that deleting the last item in a ``gtk.ListStore`` using
    del store[-1] works.

    Disabled due to segfault.

    :bug: #537459
    '''
    # model = gtk.ListStore(int)
    # for i in range(500):
    #    model.append((i,))
    #    del model[-1]

def test_long_liststore():
    '''
    Ensure that a ``gtk.ListStore`` for longs can hold any Python long
    value, even if it is larger than the C long type.

    :bug: #388216
    '''
    store = gtk.ListStore(long)
    store.append([189015844108])
