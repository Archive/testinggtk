'''
Tests for the ``gtk.Builder`` class.
'''
import gtk

def test_default_attributes():
    b = gtk.Builder()
    assert not b.get_property('translation-domain')
    assert not b.get_translation_domain()
    assert b.get_objects() == []

def test_load_combo_box():
    xml = '''
    <interface>
        <object class = "GtkComboBox" id = "foo">
        </object>
    </interface>
    '''
    b = gtk.Builder()
    b.add_from_string(xml)
    combo = b.get_object('foo')
    assert isinstance(combo, gtk.ComboBox)

def test_connect_button_signal():
    '''
    Tests that ``connect_signals`` work.
    '''
    xml = '''
    <interface>
        <object class = "GtkButton" id = "foo">
            <signal name = "clicked" handler = "clicked_cb"/>
        </object>
    </interface>
    '''
    def clicked_cb(*args):
        print 'clicked'
    b = gtk.Builder()
    b.add_from_string(xml)
    b.connect_signals(locals())
    button = b.get_object('foo')
    button.clicked()
