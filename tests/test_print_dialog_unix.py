'''
Tests for the ``gtkunixprint.PrintUnixDialog`` class.
'''
import gtkunixprint
import utils

def test_current_page_number():
    '''
    Ensure that getting and setting the current page number attribute
    works as expected.
    '''
    printdialog = gtkunixprint.PrintUnixDialog()
    for page in range(1, 200):
        printdialog.set_current_page(page)
        assert page == printdialog.get_current_page()

def test_page_range_parsing():
    '''
    This test ensure that the dialog can parse various page range
    strings.

    This would be so much easier if the page-range parser was split
    out somehow.

    :bug: #421998
    '''
    strings_and_ranges = [
        ('2-3', [(1, 2)]),
        ('2-3, 5-10', [(1, 2), (4, 9)]),
        ('2   -3', [(1, 2)])
        ]
    path = 'VBox.VBox.Notebook.VBox.HBox.VBox.Alignment.Table.Entry'
    path = path.split('.')
    dialog = gtkunixprint.PrintUnixDialog()

    for string, expected_range in strings_and_ranges:
        entry = utils.gtk_container_find_child(dialog, path)
        entry.set_text(string)
        settings = dialog.get_settings()
        assert expected_range == settings.get_page_ranges()
