'''
Tests for the ``gtk.ImageMenuItem`` class.
'''
import gtk

def test_default_attributes():
    item = gtk.ImageMenuItem()
    assert not item.get_image()

def test_set_get_image():
    label = gtk.Label('hello')
    item = gtk.ImageMenuItem()
    item.set_image(label)
    assert item.get_image() == label
    item.set_image(None)
    assert not item.get_image()

def test_forall():
    def forall_cb(widget):
        assert isinstance(widget, gtk.Label)
    item = gtk.ImageMenuItem()
    item.set_image(gtk.Label('foobar'))
    item.forall(forall_cb)

def test_foreach():
    def foreach_cb(widget):
        assert isinstance(widget, gtk.Label)
    item = gtk.ImageMenuItem()
    item.set_image(gtk.Label('foobar'))
    item.foreach(foreach_cb)

def test_forall_with_container_child():
    '''
    Ensure that ``forall`` finds two "children" of the
    ``gtk.ImageMenuItem``, first the ``gtk.Label`` and the the
    ``gtk.Image`` set with ``set_image``.
    '''
    forall_widgets = []
    def forall_cb(widget):
        forall_widgets.append(widget)
    item = gtk.ImageMenuItem("Label text")
    item.set_image(gtk.Image())
    item.forall(forall_cb)
    assert len(forall_widgets) == 2
    assert isinstance(forall_widgets[0], gtk.Label)
    assert isinstance(forall_widgets[1], gtk.Image)

def test_foreach_with_container_child():
    '''
    Ensure that ``foreach`` only finds one children of the
    ``gtk.ImageMenuItem``. The ``gtk.Image`` set with ``set_image`` is
    an internal child.

    :bug: #534979
    '''
    foreach_widgets = []
    def foreach_cb(widget):
        foreach_widgets.append(widget)
    item = gtk.ImageMenuItem("Label text")
    item.set_image(gtk.Image())
    item.foreach(foreach_cb)
    assert len(foreach_widgets) == 1
    assert isinstance(foreach_widgets[0], gtk.Label)
    
