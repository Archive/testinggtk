'''
Tests for the ``gtk.gdk.DragContext`` class.
'''
import gtk
from gtk import gdk
from tests import utils

def test_default_attributes():
    dc = gdk.DragContext()
    assert not dc.get_source_widget()
    assert not dc.is_source
    assert not dc.targets
    assert not dc.source_window
    assert not dc.dest_window
    assert dc.action == 0
    assert dc.actions == 0
    assert dc.suggested_action == 0
    assert dc.start_time == 0
    assert dc.protocol == 0

def test_set_is_source_attribute():
    '''
    Ensure that a ``TypeError`` is raised if the ``is_source``
    attribute is written to.
    '''
    dc = gdk.DragContext()
    try:
        dc.is_source = True
        assert False
    except TypeError:
        assert True

def test_drag_get_selection():
    '''
    Ensure that calling ``drag_get_selection`` works as expected.
    
    :bug: #533745
    '''
    # Disabled due to segfault
    #dc = gdk.DragContext()
    #dc.drag_get_selection()

def test_get_source_widget():
    dc = gdk.DragContext()
    assert not dc.get_source_widget()

@utils.fail_on_warnings
def test_set_icon_pixmap_no_warning():
    '''
    Ensure that ``set_icon_pixmap`` can be invoked on a newly
    constructed ``gtk.gdk.DragContext`` without producing GtkWarnings.

    :bug: #539403
    '''
    pix = gdk.Pixmap(None, 100, 100, depth = 32)
    dc = gdk.DragContext()
    cmap = gdk.colormap_get_system()
    dc.set_icon_pixmap(cmap, pix, pix, 0, 0)

@utils.fail_on_warnings
def test_set_icon_stock_no_warning():
    '''
    Ensure that ``set_icon_stock`` can be called on a newly created
    ``gdk.DragContext`` without triggering a GtkWarning.

    :bug: #539403
    '''
    dc = gdk.DragContext()
    dc.set_icon_stock('stock-id', 0, 0)

@utils.fail_on_warnings
def test_set_icon_widget_no_warning():
    '''
    :bug: #539403
    '''
    dc = gdk.DragContext()
    dc.set_icon_widget(gtk.Label(), 0, 0)

@utils.fail_on_warnings
def test_set_icon_pixbuf_no_warning():
    '''
    :bug: #539403
    '''
    pixbuf = gdk.Pixbuf(gdk.COLORSPACE_RGB, False, 8, 10, 10)
    dc = gdk.DragContext()
    dc.set_icon_pixbuf(pixbuf, 0, 0)

@utils.fail_on_warnings
def test_set_icon_name_no_warning():
    '''
    :bug: #539403
    '''
    dc = gdk.DragContext()
    dc.set_icon_name('foo', 0, 0)

@utils.fail_on_warnings    
def test_set_icon_default_no_warning():
    '''
    :bug: #539403
    '''
    dc = gdk.DragContext()
    dc.set_icon_default()

@utils.fail_on_warnings
def test_set_icon_pixmap_none_mask():
    '''
    Ensure that ``set_icon_pixmap`` can be invoked successfully with
    ``None`` as the mask.

    :bug: #497781
    '''
    pix = gdk.Pixmap(None, 100, 100, depth = 32)
    dc = gdk.DragContext()
    cmap = gdk.colormap_get_system()
    dc.set_icon_pixmap(cmap, pix, None, 0, 0)
