'''
Tests for the ``gdk.Event`` class.
'''
from gtk import gdk

def test_set_int_coordinate():
    '''
    Ensure that setting the x or y attribute of a ``gdk.Event`` to an
    integer value results in a TypeError.
    '''
    ev = gdk.Event(gdk.MOTION_NOTIFY)
    try:
        ev.x = 10
        assert False
    except TypeError:
        assert True
    try:
        ev.y = 10
        assert False
    except TypeError:
        assert True

def test_get_root_coords():
    ev = gdk.Event(gdk.MOTION_NOTIFY)
    ev.x_root = 100.0
    ev.y_root = 120.0
    ev.get_root_coords() == (100.0, 120.0)
