'''
Tests for the ``gdk.PixbufAnimation`` class.
'''
from gtk import gdk

def test_iter_on_currently_loading_frame_on_empty_anim():
    '''
    Ensure that ``iter.on_currently_loading_frame()`` returns ``True``
    on an empty animation.
    '''
    anim = gdk.PixbufSimpleAnim(100, 200, 10)
    iter = anim.get_iter()
    assert iter.on_currently_loading_frame() 

def test_iter_on_currently_loading_frame_one_frame():
    '''
    Ensure that ``iter.on_currently_loading_frame()`` returns ``True``
    if the animation has only one frame.
    '''
    anim = gdk.PixbufSimpleAnim(100, 200, 10)
    anim.add_frame(gdk.Pixbuf(gdk.COLORSPACE_RGB, False, 8, 400, 300))
    iter = anim.get_iter()
    assert iter.on_currently_loading_frame() 

def test_iter_on_currently_loading_frame_not_last():
    '''
    Ensure that ``iter.on_currently_loading_frame()`` returns
    ``False`` if the pixbuf animation iterator isn't on the last
    frame in a non-empty animation.
    '''
    anim = gdk.PixbufSimpleAnim(100, 200, 40)
    for x in range(5):
        pixbuf = gdk.Pixbuf(gdk.COLORSPACE_RGB, False, 8, 400, 300)
        anim.add_frame(pixbuf)

    time = 0.0
    iter = anim.get_iter(time)
    for x in range(4):
        assert not iter.on_currently_loading_frame()
        time += iter.get_delay_time() / 1000.0
        # In case we fall on an edge between two frames
        while not iter.advance(time):
            time += 0.001
