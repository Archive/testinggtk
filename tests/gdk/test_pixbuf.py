'''
Tests for the ``gtk.gdk.Pixbuf`` class
'''
from gtk import gdk

def test_save_to_callback():
    '''
    Ensure that the callback function is called in
    ``save_to_callback``.
    '''
    called = [False]
    def func(buf):
        called[0] = True
    pixbuf = gdk.Pixbuf(gdk.COLORSPACE_RGB, False, 8, 100, 100)
    pixbuf.save_to_callback(func, 'jpeg')
    assert called[0]

def test_save_to_callback_options_none():
    '''
    Ensure that ``None`` can be specified explicitly as the
    ``options`` argument to ``save_to_callback``.

    :bug: #472462
    '''
    pixbuf = gdk.Pixbuf(gdk.COLORSPACE_RGB, False, 8, 100, 100)
    pixbuf.save_to_callback(lambda x: x, 'jpeg', options = None)

def test_save_options_none():
    '''
    Ensure that ``None`` can be specified explicitly as the
    ``options`` argument to ``save``.

    :bug: #472462
    '''
    pixbuf = gdk.Pixbuf(gdk.COLORSPACE_RGB, False, 8, 100, 100)
    pixbuf.save('foo', 'png', options = None)

def test_save_to_callback_user_data_none():
    '''
    Ensure that ``None`` can be specified explicitly as the
    ``user_data`` argument to ``save_to_callback``.

    :bug: #472462
    '''
    pixbuf = gdk.Pixbuf(gdk.COLORSPACE_RGB, False, 8, 100, 100)
    pixbuf.save_to_callback(lambda x: x, 'jpeg', user_data = None)

def test_save_options_wrong():
    '''
    Ensure that a ``TypeError`` is raised if the ``options`` argument
    to ``save`` is not ``None`` or a mapping.
    '''
    pixbuf = gdk.Pixbuf(gdk.COLORSPACE_RGB, False, 8, 100, 100)
    try:
        pixbuf.save('foo', 'png', options = 1234)
        assert False
    except TypeError:
        assert True

def test_save_to_callback_options_wrong():
    '''
    Ensure that a ``TypeError`` is raised if the ``options`` argument
    to ``save_to_callback`` is not ``None`` or a mapping.
    '''
    pixbuf = gdk.Pixbuf(gdk.COLORSPACE_RGB, False, 8, 100, 100)
    try:
        pixbuf.save_to_callback(lambda x: x, 'png', options = 1234)
        assert False
    except TypeError:
        assert True

def test_new_from_data_zero_size():
    '''
    Attempts to create zero-size pixbufs should raise RuntimeError.
    
    :bug: #584769
    '''
    try:
        gdk.pixbuf_new_from_data('', gdk.COLORSPACE_RGB, False, 8, 0, 0, 0)
        assert False
    except RuntimeError:
        assert True

