'''
Tests for the ``gtk.gdk.PixbufSimpleAnim`` class.
'''
from gtk import gdk

def test_default_attributes():
    anim = gdk.PixbufSimpleAnim(100, 200, 10)
    assert anim.get_width() == 100
    assert anim.get_height() == 200
    iter = anim.get_iter()
    assert not iter.get_pixbuf()
    assert iter.get_delay_time() == -1
