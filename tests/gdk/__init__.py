'''
Tests for the ``gtk.gdk`` package.
'''
import gobject
from gtk import gdk

def test_modifier_type():
    '''
    Ensure that ``gdk.ModifierType`` is a ``GFlags`` type.
    '''
    gobject.type_parent(gdk.ModifierType) == gobject.GFlags

def test_list_drag_proto_enum():
    '''
    Ensure that all drag proto constants are defined in the gdk
    module.
    '''
    assert hasattr(gdk, 'DRAG_PROTO_MOTIF')
    assert hasattr(gdk, 'DRAG_PROTO_XDND')
    assert hasattr(gdk, 'DRAG_PROTO_ROOTWIN')
    assert hasattr(gdk, 'DRAG_PROTO_NONE')
    assert hasattr(gdk, 'DRAG_PROTO_WIN32_DROPFILES')
    assert hasattr(gdk, 'DRAG_PROTO_OLE2')
    assert hasattr(gdk, 'DRAG_PROTO_LOCAL')

def test_list_action_enum():
    '''
    Ensure that all action enumeration values are present in the gdk
    module.
    '''
    assert hasattr(gdk, 'ACTION_DEFAULT')
    assert hasattr(gdk, 'ACTION_COPY')
    assert hasattr(gdk, 'ACTION_MOVE')
    assert hasattr(gdk, 'ACTION_LINK')
    assert hasattr(gdk, 'ACTION_PRIVATE')
    assert hasattr(gdk, 'ACTION_ASK')

def test_list_event_enum():
    # Not all listed
    assert hasattr(gdk, 'NOTHING')
    assert hasattr(gdk, 'DELETE')
    assert hasattr(gdk, 'DESTROY')
    assert hasattr(gdk, 'EXPOSE')
    assert hasattr(gdk, 'MOTION_NOTIFY')
    assert hasattr(gdk, 'BUTTON_PRESS')
    assert hasattr(gdk, '_2BUTTON_PRESS')
    assert hasattr(gdk, '_3BUTTON_PRESS')
    assert hasattr(gdk, 'BUTTON_RELEASE')
