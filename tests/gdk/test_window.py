'''
Tests for the ``gtk.gdk.Window`` class.
'''
from gtk import gdk

def test_window_lookup_for_display_id0():
    '''
    There should be no window with system ID 0 on the default display.
    '''
    display = gdk.display_get_default()
    window = gdk.window_lookup_for_display(display, 0)
    assert not window

def test_window_foreign_new_for_display_id0():
    display = gdk.display_get_default()
    window = gdk.window_foreign_new_for_display(display, 0)
    assert not window

def test_get_constructor_specified_position():
    '''
    Ensure that the position is correct after the ``gdk.Window`` has
    been created.
    '''
    window = gdk.Window(None,
                        100, 100,
                        gdk.WINDOW_TOPLEVEL,
                        0,
                        gdk.INPUT_OUTPUT,
                        x = 100, y = 100)
    assert window.get_position() == (100, 100)

def test_set_get_position_unrealized():
    '''
    Test getting and setting the position of a top level unrealized
    window.

    A non override redirect window doesn't update its position when it
    is moved. That may or may not be a bug.
    '''
    window = gdk.Window(None,
                        100, 100,
                        gdk.WINDOW_TOPLEVEL,
                        0,
                        gdk.INPUT_OUTPUT)
    window.move(300, 300)
    assert window.get_position() == (0, 0)

def test_move_unrealized_override_redirect_window():
    '''
    Ensure that moving an unrealized override redirect window updates
    that windows position.
    '''
    window = gdk.Window(None,
                        100, 100,
                        gdk.WINDOW_TOPLEVEL,
                        0,
                        gdk.INPUT_OUTPUT)
    window.set_override_redirect(True)
    window.move(300, 300)
    assert window.get_position() == (300, 300)

def test_get_pointer_unrealized():
    '''
    Ensure that ``get_pointer`` returns a three tuple where the first
    two elements are integers and the third is a GDK modifier mask.
    '''
    window = gdk.Window(None,
                        100, 100,
                        gdk.WINDOW_TOPLEVEL,
                        0,
                        gdk.INPUT_OUTPUT)
    x, y, modifier = window.get_pointer()
    assert isinstance(x, int)
    assert isinstance(y, int)
    assert isinstance(modifier, gdk.ModifierType)

def test_get_origin():
    window = gdk.Window(None,
                        100, 100,
                        gdk.WINDOW_TOPLEVEL,
                        0,
                        gdk.INPUT_OUTPUT,
                        x = 120, y = 80)
    x, y = window.get_origin()
    assert x == 120
    assert y == 80

def test_default_update_area():
    '''
    The whole window area is invalidated when shown.

    :commit: 9f2c97dd803aae318180fb585ea12d7996282a02
    '''
    window = gdk.Window(None,
                        100, 100,
                        gdk.WINDOW_TOPLEVEL,
                        0,
                        gdk.INPUT_OUTPUT)
    assert not window.get_update_area()
    window.show()
    area = window.get_update_area().get_clipbox()
    assert area == gdk.Rectangle(0, 0, 100, 100)

def test_cant_invalidate_unmapped_window():
    '''
    An unmapped ``gdk.Window`` cant be invalidated.

    :bug: #540991
    '''
    window = gdk.Window(None,
                        100, 100,
                        gdk.WINDOW_TOPLEVEL,
                        0,
                        gdk.INPUT_OUTPUT)
    assert not window.get_update_area()
    window.invalidate_rect(gdk.Rectangle(0, 0, 50, 50), False)
    assert not window.get_update_area()

def test_invalidate_mapped_window():
    '''
    Ensure that a mapped ``gdk.Window`` can be invalidated as
    expected.
    '''
    window = gdk.Window(None,
                        100, 100,
                        gdk.WINDOW_TOPLEVEL,
                        0,
                        gdk.INPUT_OUTPUT)
    window.show()
    gdk.window_process_all_updates()

    window.invalidate_rect(gdk.Rectangle(0, 0, 50, 50), False)
    region = window.get_update_area()
    area = region.get_clipbox()
    assert area.x == 0
    assert area.y == 0
    assert area.width == 50
    assert area.height == 50

def test_create_child_window():
    window = gdk.Window(None,
                        100, 100,
                        gdk.WINDOW_CHILD,
                        0,
                        gdk.INPUT_OUTPUT)
    assert window.get_window_type() == gdk.WINDOW_CHILD

def test_show_and_hide():
    '''
    Ensure that showing and hiding a window changes its ``is_visible``
    attribute.
    '''
    window = gdk.Window(None,
                        100, 100,
                        gdk.WINDOW_TOPLEVEL,
                        0,
                        gdk.INPUT_OUTPUT)
    assert not window.is_visible()
    window.show()
    assert window.is_visible()
    window.hide()
    assert not window.is_visible()

def test_reparent_hides_window():
    '''
    Ensure that reparenting a window hides it.

    :bug: #546817, #548993
    '''
    parent = gdk.Window(None,
                        100, 100,
                        gdk.WINDOW_TOPLEVEL,
                        0,
                        gdk.INPUT_OUTPUT)
    child = gdk.Window(None,
                       100, 100,
                       gdk.WINDOW_TOPLEVEL,
                       0,
                       gdk.INPUT_OUTPUT)
    parent.show()
    child.show()
    child.reparent(parent, 0, 0)
    assert not child.is_visible()

def test_reparent_changes_origin():
    '''
    Ensure that reparenting a window changes its origin.
    '''
    parent = gdk.Window(None,
                        100, 100,
                        gdk.WINDOW_TOPLEVEL,
                        0,
                        gdk.INPUT_OUTPUT)
    window = gdk.Window(None,
                        20, 20,
                        gdk.WINDOW_TOPLEVEL,
                        0,
                        gdk.INPUT_OUTPUT)
    window.reparent(parent, 55, 66)
    assert window.get_origin() == (55, 66)

def test_process_all_updates():
    '''
    Calling ``gdk.window_process_all_updates`` clears the update area
    of all windows.
    '''
    window = gdk.Window(None,
                        20, 20,
                        gdk.WINDOW_TOPLEVEL,
                        0,
                        gdk.INPUT_OUTPUT)
    window.show()
    assert window.get_update_area()
    gdk.window_process_all_updates()
    assert not window.get_update_area()
