'''
Tests for the ``gtk.gdk.Region`` class.
'''
from gtk import gdk

def test_empty_region():
    region = gdk.Region()
    assert region.empty()
    assert region.equal(gdk.Region())
    assert region.get_clipbox() == gdk.Rectangle(0, 0, 0, 0)
    
