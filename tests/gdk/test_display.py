'''
Tests for the ``gdk.Display`` class.
'''
from gtk import gdk

def test_default_attributes():
    display = gdk.display_get_default()
    assert isinstance(display.get_name(), str)
    assert isinstance(display.get_n_screens(), int)
    assert isinstance(display.get_default_screen(), gdk.Screen)
    assert isinstance(display.list_devices(), list)
    assert isinstance(display.get_core_pointer(), gdk.Device)
    assert display.supports_cursor_alpha() in (True, False)
    assert display.supports_cursor_color() in (True, False)
    
