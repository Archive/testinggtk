'''
Tests for the ``gtk.Statusbar`` class.
'''
import gtk

def test_default_attributes():
    bar = gtk.Statusbar()
    assert bar.get_has_resize_grip()

def test_check_children():
    '''
    Ensure that a newly constructed ``gtk.Statusbar`` has one child
    which is an instance of a ``gtk.Frame``.
    '''
    bar = gtk.Statusbar()
    children = bar.get_children()
    assert len(children) == 1
    assert isinstance(children[0], gtk.Frame)

