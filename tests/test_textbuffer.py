'''
Tests for the ``gtk.TextBuffer`` class.
'''
import gtk

def test_end_iter():
    buffer = gtk.TextBuffer()
    buffer.set_text("A" * 5555)
    assert buffer.get_end_iter().get_offset() == 5555
