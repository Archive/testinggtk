'''
Tests for the ``gtk.WindowGroup`` class.
'''
import gtk

def test_add_none_window():
    '''
    Ensure that a ``TypeError`` is raised if an attempt to add
    ``None`` to a ``gtk.WindowGroup`` is made.
    '''
    group = gtk.WindowGroup()
    try:
        group.add_window(None)
        assert False
    except TypeError:
        assert True
