'''
Tests for the ``gtk.IconTheme`` class.
'''
import gtk

def test_wrapper_complete():
    '''
    Ensure that all required public attributes are wrapped.

    :bug: #539481
    '''
    it = gtk.IconTheme()
    assert hasattr(it, 'set_screen')
    assert hasattr(it, 'set_search_path')
    assert hasattr(it, 'get_search_path')
    assert hasattr(it, 'append_search_path')
    assert hasattr(it, 'prepend_search_path')
    assert hasattr(it, 'set_custom_theme')
    assert hasattr(it, 'has_icon')
    assert hasattr(it, 'lookup_icon')
    assert hasattr(it, 'lookup_by_gicon')
    assert hasattr(it, 'load_icon')
    assert hasattr(it, 'choose_icon')
    assert hasattr(it, 'list_contexts')
    assert hasattr(it, 'list_icons')
    assert hasattr(it, 'get_example_icon_name')
    assert hasattr(it, 'rescan_if_needed')
    assert hasattr(it, 'get_icon_sizes')
