'''
Tests for the ``gtk.Settings`` class.
'''
import gtk

def test_sound_settings():
    '''
    Ensure that the three sound-related settings
    ``gtk-sound-theme-name``, ``gtk-enable-input-feedback-sounds`` and
    ``gtk-enable-event-sounds`` are present in the default
    ``gtk.Settings`` object.

    :bug: #539790
    '''
    settings = gtk.settings_get_default()
    prop = settings.get_property('gtk-sound-theme-name')
    assert isinstance(prop, str)
    prop = settings.get_property('gtk-enable-input-feedback-sounds')
    assert prop in (True, False)
    prop = settings.get_property('gtk-enable-event-sounds')
    assert prop in (True, False)

def test_tooltip_settings():
    settings = gtk.settings_get_default()
    prop = settings.get_property('gtk-tooltip-browse-timeout')
    assert isinstance(prop, int)
    prop = settings.get_property('gtk-tooltip-timeout')
    assert isinstance(prop, int)
    prop = settings.get_property('gtk-tooltip-browse-mode-timeout')
    assert isinstance(prop, int)


def test_misc_settings():
    settings = gtk.settings_get_default()
    prop = settings.get_property('gtk-touchscreen-mode')
    assert prop in (True, False)
