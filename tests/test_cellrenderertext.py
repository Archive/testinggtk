'''
Tests for the ``gtk.CellRendererText`` class.
'''
import gtk
from gtk import gdk

def test_default_attributes():
    cr = gtk.CellRendererText()
    assert not cr.get_property('editable')
    assert not cr.get_property('text')

    # gtk.CellRenderer properties
    bg = cr.get_property('cell-background-gdk')
    assert bg.red == 0
    assert bg.green == 0
    assert bg.blue == 0

    assert cr.get_property('width') == -1
    assert cr.get_property('height') == -1

    # gtk.CellRendererText overrides these defaults.
    assert cr.get_property('xalign') == 0.0
    assert cr.get_property('yalign') == 0.5

    assert cr.get_property('xpad') == 2
    assert cr.get_property('ypad') == 2

    assert not cr.get_property('cell-background-set')
    assert not cr.get_property('is-expanded')
    assert not cr.get_property('is-expander')

    assert cr.get_property('mode') == gtk.CELL_RENDERER_MODE_INERT
    assert cr.get_property('sensitive') == True
    assert cr.get_property('visible') == True

def test_editing_started_signal():
    '''
    Ensure that the ``editing-started`` signal is emitted when
    ``start_editing`` is called on an editable
    ``gtk.CellRendererText``.
    '''
    n_emits = [0]
    def editing_started_cb(*args):
        n_emits[0] += 1
    cr = gtk.CellRendererText()
    cr.connect('editing-started', editing_started_cb)

    cr.set_property('editable', True)
    cr.start_editing(gdk.Event(gdk.NOTHING),
                     gtk.Button(),
                     '',
                     (0, 0, 10, 10),
                     (0, 0, 10, 10),
                     0)
    assert n_emits[0] == 1

    # It won't be emitted again if the cell renderer isn't editable.
    cr.set_property('editable', False)
    cr.start_editing(gdk.Event(gdk.NOTHING),
                     gtk.Button(),
                     '',
                     (0, 0, 10, 10),
                     (0, 0, 10, 10),
                     0)
    assert n_emits[0] == 1

def test_set_get_text():
    cr = gtk.CellRendererText()
    cr.set_property('text', 'foo')
    assert cr.get_property('text') == 'foo'
    cr.set_property('text', None)
    assert not cr.get_property('text')
