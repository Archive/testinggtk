'''
Tests for the ``gtk.PrintSettings`` class.
'''
import gtk

def test_default_values():
    '''
    Ensure that the default values of ``gtk.PrintSettings``
    are correct.
    '''
    ps = gtk.PrintSettings()
    assert ps.get_scale() == 100.0
    assert ps.get_print_pages() == gtk.PRINT_PAGES_ALL
    assert ps.get_page_ranges() == []
