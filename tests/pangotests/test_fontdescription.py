'''
Tests for the ``pango.FontDescription`` class.
'''
import pango

def test_default_attributes():
    fd = pango.FontDescription()
    assert fd.get_gravity() == pango.GRAVITY_SOUTH
    assert fd.get_set_fields() == 0
    assert fd.get_size() == 0
    assert fd.get_stretch() == pango.STRETCH_NORMAL
    assert fd.get_style() == pango.STYLE_NORMAL
    assert fd.get_variant() == pango.VARIANT_NORMAL
    assert fd.get_weight() == pango.WEIGHT_NORMAL
    assert not fd.get_family()
    assert not fd.get_size_is_absolute()

def test_get_font_description_from_string():
    fd = pango.FontDescription('Sans 12px')
    assert fd.get_family() == 'Sans'
    assert fd.get_size() / pango.SCALE == 12

def test_font_description_from_0_size_string():
    fd = pango.FontDescription('Nimbus Sans L Regular 0')
    assert fd.get_size() == 0
    assert fd.get_family() == 'Nimbus Sans L Regular'

    fd = pango.FontDescription('Sans')
    assert fd.get_family() == 'Sans'
    assert fd.get_size() == 0

def test_negative_size_from_string():
    '''
    Ensure that a font string with a negative size gets it clamped to
    0.
    '''
    for x in -33, -159.99, -0.05, -12345678:
        fd = pango.FontDescription('Sans %.2f' % x)
        assert fd.get_size() == 0
