'''
Tests for the ``pango.FontSet`` class.
'''
import pango
import gtk

def test_default_attributes():
    ctx = gtk.Label().create_pango_context()
    fd = pango.FontDescription('Sans 10')
    fontset = ctx.load_fontset(fd, ctx.get_language())
    assert fontset
