'''
Tests for the ``pango.Layout`` class.
'''
import gtk
import pango

def test_default_attributes():
    widget = gtk.Button()
    context = widget.get_pango_context()
    layout = pango.Layout(context)

    assert layout.get_context() == context
    assert not layout.get_attributes()
    assert not layout.get_text()
    assert layout.get_width() == -1
