'''
Tests for the ``pango.Context`` class.
'''
import gtk
import pango
from tests import utils

def test_instantiate_context():
    '''
    The ``pango.Context`` class is instantiatable.

    :bug: #550855
    '''
    ctx = pango.Context()

def test_widget_default_attributes():
    '''
    Tests the default attributes of a ``pango.Context`` created from a
    ``gtk.Widget`` using ``create_pango_context``.
    '''
    ctx = gtk.Label().create_pango_context()
    assert ctx.get_base_dir() == pango.DIRECTION_LTR
    lang = ctx.get_language()
    # yeah..
    assert lang.to_string() == 'sv-se'
    fd = ctx.get_font_description()
    assert fd.get_family() == 'Verdana'
    assert fd.get_size() / pango.SCALE == 10

def test_load_fontset_none_language():
    '''
    Ensure that a ``TypeError`` is raised if ``load_fontset`` is
    called with ``None`` as the ``pango.Language``.
    '''
    ctx = gtk.Label().create_pango_context()
    fd = pango.FontDescription('Sans 12px')
    try:
        ctx.load_fontset(fd, None)
        assert False
    except TypeError:
        assert True

@utils.pass_on_warnings        
def test_load_fontset():
    '''
    Ensure that a warning is printed if a ``pango.FontDescription``
    whose font has 0 size is being loaded to a ``pango.Context``.
    '''
    ctx = gtk.Label().create_pango_context()
    fd = pango.FontDescription('Sans 0')
    ctx.load_fontset(fd, ctx.get_language())
