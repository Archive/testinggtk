'''
Test for gobject properties.
'''
import gobject

def test_default_attributes():
    prop = gobject.property()
    assert not prop.default
    assert not prop.name
    assert not prop.maximum
    assert not prop.minimum
    assert prop.nick == ''
    assert prop.blurb == ''
    assert prop.type == gobject.TYPE_PYOBJECT
    assert prop.flags == gobject.PARAM_READWRITE
    
    type, s1, s2, flags = prop.get_pspec_args()
    assert type == gobject.TYPE_PYOBJECT
    # What does these three items mean?
    assert s1 == ''
    assert s2 == ''
    assert flags == gobject.PARAM_READWRITE

def test_list_param_flags():
    '''
    Ensure that all available parameter specification flags are listed
    in the gobject module.
    '''
    assert hasattr(gobject, 'PARAM_CONSTRUCT')
    assert hasattr(gobject, 'PARAM_CONSTRUCT_ONLY')
    assert hasattr(gobject, 'PARAM_LAX_VALIDATION')
    assert hasattr(gobject, 'PARAM_READABLE')
    assert hasattr(gobject, 'PARAM_READWRITE')
    assert hasattr(gobject, 'PARAM_WRITABLE')

def test_create_char_property():
    '''
    Check that it is possible to create a property of type
    ``gobject.TYPE_CHAR``.

    :bug: #539355
    '''
    prop = gobject.property(type = gobject.TYPE_CHAR)
    assert prop.type == gobject.TYPE_CHAR
    type, nick, blurb, flags = prop.get_pspec_args()

def test_create_pointer_property():
    '''
    Check that it is possible to create a property of type
    ``gobject.TYPE_POINTER``.

    :bug: #539355
    '''
    prop = gobject.property(type = gobject.TYPE_POINTER)
    assert prop.type == gobject.TYPE_POINTER
    type, nick, blurb, flags = prop.get_pspec_args()
