'''
Tests for the gobject module.
'''
import gobject

def test_glib_version():
    major, minor, micro = gobject.glib_version
    assert major == 2
    assert minor >= 17
    assert micro >= 0

def test_install_properties():
    class MySub(gobject.GObject):
        prop1 = gobject.property(type = int)
    obj = MySub()
    obj.set_property('prop1', 100)
    assert obj.get_property('prop1') == 100

def test_install_enum_property():
    '''
    :bug: #539355
    '''
    class MySub(gobject.GObject):
        prop = gobject.property(type = gobject.TYPE_ENUM)

def test_markup_escape_text():
    '''
    Ensure that ``gobject.markup_escape_text`` works as expected.

    :bug: #540696
    '''
    assert gobject.markup_escape_text('Hi') == 'Hi'

def test_list_types():
    '''
    Ensure that all required object types are defined in the gobject
    module.
    '''
    assert hasattr(gobject, 'TYPE_BOOLEAN')
    assert hasattr(gobject, 'TYPE_BOXED')
    assert hasattr(gobject, 'TYPE_CHAR')
    assert hasattr(gobject, 'TYPE_DOUBLE')
    assert hasattr(gobject, 'TYPE_ENUM')
    assert hasattr(gobject, 'TYPE_FLAGS')
    assert hasattr(gobject, 'TYPE_FLOAT')
    assert hasattr(gobject, 'TYPE_GSTRING')
    assert hasattr(gobject, 'TYPE_INT')
    assert hasattr(gobject, 'TYPE_INT64')
    assert hasattr(gobject, 'TYPE_INTERFACE')
    assert hasattr(gobject, 'TYPE_INVALID')
    assert hasattr(gobject, 'TYPE_LONG')
    assert hasattr(gobject, 'TYPE_NONE')
    assert hasattr(gobject, 'TYPE_OBJECT')
    assert hasattr(gobject, 'TYPE_PARAM')
    assert hasattr(gobject, 'TYPE_POINTER')
    assert hasattr(gobject, 'TYPE_PYOBJECT')
    assert hasattr(gobject, 'TYPE_STRING')
    assert hasattr(gobject, 'TYPE_UCHAR')
    assert hasattr(gobject, 'TYPE_UINT')
    assert hasattr(gobject, 'TYPE_UINT64')
    assert hasattr(gobject, 'TYPE_ULONG')
    assert hasattr(gobject, 'TYPE_UNICHAR')

