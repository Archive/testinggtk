'''
Tests for gobject signals.
'''
import gobject

def test_signal_without_type():
    '''
    Ensure that a ``TypeError`` is raised if the ``type`` parameter is
    ``None``.

    :bug: #540376
    '''
    try:
        gobject.signal_new('test',
                           None,
                           0,
                           gobject.TYPE_NONE,
                           (gobject.TYPE_LONG,))
        assert False
    except TypeError:
        assert True

class Test45(gobject.GObject):
    pass
gobject.type_register(Test45)

def test_new_signal():
    '''
    Ensure that creating a new signal works as expected.
    '''
    id = gobject.signal_new('test',
                            Test45,
                            0,
                            gobject.TYPE_NONE,
                            (gobject.TYPE_LONG,))
    assert isinstance(id, int)

def test_emit_new_signal():
    value = [None]
    def test2_cb(obj, arg):
        value[0] = arg
    gobject.signal_new('test2',
                       Test45,
                       0,
                       gobject.TYPE_NONE,
                       (gobject.TYPE_LONG,))
    t = Test45()
    t.connect('test2', test2_cb)
    t.emit('test2', 1)
    assert value[0] == 1
    
