'''
Tests for the ``gobject.GObject`` class.
'''
import gobject
from tests import utils

@utils.fail_on_warnings
def test_multiple_freeze():
    '''
    Ensure that freezing notify signals multiple times works as
    expected.
    '''
    obj = gobject.GObject()
    for x in range(10):
        obj.freeze_notify()

@utils.pass_on_warnings
def test_multiple_thaw():
    '''
    Ensure that thaw notifying an unfrozen ``gobject.GObject`` prints
    a warning.

    :bug: #539553
    '''
    obj = gobject.GObject()
    obj.thaw_notify()

@utils.pass_on_warnings
def test_handler_block_invalid_handler_id():
    '''
    Ensure that a warning is printed when ``handler_block`` is called
    with a non-existent ``handler_id``.
    '''
    obj = gobject.GObject()
    obj.handler_block(123)

def test_notify_signal():
    '''
    Ensure that the ``notify`` signal is emitted properly when a
    property is being modified.
    '''
    class Sub46(gobject.GObject):
        prop = gobject.property(type = int)
    gobject.type_register(Sub46)

    cb_args = []
    def notify_cb(obj, prop_spec):
        cb_args.append((obj, prop_spec))

    obj = Sub46()
    obj.connect('notify', notify_cb)
    obj.set_property('prop', 99)

    cb_obj, cb_prop_spec = cb_args[0]
    assert cb_obj == obj
    assert cb_prop_spec.name == 'prop'
    assert cb_prop_spec.value_type == gobject.TYPE_INT

def test_block_signal():
    '''
    Test blocking a signal by its ``handler_id``.
    '''
    emitted = [False]
    def notify_cb(*args):
        emitted[0] = True

    class Sub47(gobject.GObject):
        prop = gobject.property(type = int)
    gobject.type_register(Sub47)
    obj = Sub47()

    handler_id = obj.connect('notify', notify_cb)
    obj.handler_block(handler_id)
    obj.set_property('prop', 99)
    assert not emitted[0]

def test_is_freeze_cumulative():
    obj = gobject.GObject()
    for x in range(10):
        obj.freeze_notify()
    for x in range(10):
        obj.thaw_notify()
