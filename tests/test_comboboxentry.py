'''
Tests for the ``gtk.ComboBoxEntry`` class.
'''
import gtk
import utils

@utils.pass_on_warnings
def test_from_model_index_out_of_bounds():
    '''
    Ensure that if a gtk.ComboBoxEntry is created with an index that
    is out of bounds for the model, then a GtkWarning is shown.
    '''
    store = gtk.ListStore(int, int, int)
    # Only index 0..2 is valid.
    gtk.combo_box_entry_new_with_model(store, 3)

def test_non_string_active_item():
    '''
    Ensure that no crash occurs if a combo box entry that refers to
    integer data in the model has its active item set.

    :bug: #423201
    '''
    # Disabled due to segfault
    # store = gtk.ListStore(int)
#     store.append([3])
#     box = gtk.combo_box_entry_new_with_model(store, 0)
#     box.set_active(0)

def test_enter_text_updates_active_iter():
    '''
    Ensure that when manually typing text identical to a string in a
    combo box entry, then that strings iterator becomes active.
    
    :bug: #156017
    '''
    combo_entry = gtk.combo_box_entry_new_text()
    combo_entry.append_text('item1')

    assert not combo_entry.get_active_iter()
    text_entry = combo_entry.get_child()
    text_entry.set_text('item1')

    iter = combo_entry.get_active_iter()
    store = combo_entry.get_model()
    assert store.get_value(iter, 0) == 'item1'
