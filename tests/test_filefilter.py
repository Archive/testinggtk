'''
Tests for the ``gtk.FileFilter`` class.
'''
import gio
import gtk

def test_default_attributes():
    ff = gtk.FileFilter()
    assert not ff.get_name()
    assert ff.get_needed() == 0

def test_filter_star_pattern():
    '''
    Ensure that the star pattern matches everything.
    '''
    ff = gtk.FileFilter()
    ff.add_pattern('*')
    file = gio.File(__file__)
    assert ff.filter((None, None, file.get_basename(), None))

def test_matching_mime():
    '''
    Ensure that matching mime type works.
    '''
    ff = gtk.FileFilter()
    ff.add_mime_type('mimetype')
    assert ff.filter((None, None, None, 'mimetype'))

def test_matching_mime_from_basetype():
    '''
    Ensure that a subtype matches a filter with that mimes
    supertype. For example, text/plain should match text/html.
    '''
    ff = gtk.FileFilter()
    ff.add_mime_type('text/plain')
    assert ff.filter((None, None, None, 'text/html'))
