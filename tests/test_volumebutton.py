'''
Tests for the ``gtk.VolumeButton``.
'''
import gtk

def test_default_attributes():
    vb = gtk.VolumeButton()
    assert vb.get_value() == 0
    assert vb.get_property('size') == gtk.ICON_SIZE_SMALL_TOOLBAR

    muted, high, low, medium = vb.get_property('icons')
    assert muted == 'audio-volume-muted'
    assert high == 'audio-volume-high'
    assert low == 'audio-volume-low'
    assert medium == 'audio-volume-medium'

    adj = vb.get_adjustment()
    assert adj.lower == 0.0
    assert adj.upper == 1.0
    assert adj.step_increment == 0.02
    
