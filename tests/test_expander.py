'''
Tests for the ``gtk.Expander`` widget.
'''
import gtk

def test_default_properties():
    expander = gtk.Expander()
    assert not expander.get_expanded()
    assert not expander.get_label()
    assert not expander.get_use_markup()
    assert not expander.get_label_widget()

def test_expander_label_includes_markup():
    '''
    Ensure that ``get_label`` returns the unparsed label text.

    :bug: #353088
    '''
    expander = gtk.Expander('<b>Hi!</b>')
    expander.set_use_markup(True)
    assert expander.get_label() == '<b>Hi!</b>'

def test_set_get_expaned():
    '''
    Ensure that the expanded property works as expected.
    '''
    expander = gtk.Expander()
    expander.add(gtk.Button())
    expander.set_expanded(True)
    assert expander.get_expanded()
    expander.set_expanded(False)
    assert not expander.get_expanded()
