'''
Tests for the ``gtk.AccelGroup`` class.
'''
import gtk
import utils

@utils.fail_on_warnings
def test_default_attributes():
    '''
    :bug: #539394
    '''
    ag = gtk.AccelGroup()
