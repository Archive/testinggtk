'''
Tests for the ``gtk.CellRendererCombo`` class.
'''
import gtk
from gtk import gdk

def test_default_attributes():
    cr = gtk.CellRendererCombo()
    assert cr.get_fixed_size() == (-1, -1)
    assert cr.get_property('has-entry')
    assert not cr.get_property('model')
    assert cr.get_property('text-column') == -1
    assert not cr.get_property('editable')

def test_start_editing_not_editable():
    '''
    Ensure that calling ``start_editing`` returns ``None`` if the cell
    renderer is not editable.
    '''
    cr = gtk.CellRendererCombo()
    assert not cr.start_editing(gdk.Event(gdk.NOTHING),
                                gtk.Button(), '',
                                gdk.Rectangle(),
                                gdk.Rectangle(),
                                0)

def test_start_editing_return_combo_entry():
    '''
    Ensure that calling ``start_editing`` on a
    ``gtk.CellRendererCombo`` that is editable, has a model and whose
    `text_column` >= 0 returns a ``gtk.ComboBoxEntry``.
    '''
    model = gtk.ListStore(str)
    cr = gtk.CellRendererCombo()
    cr.set_property('model', model)
    cr.set_property('text-column', 0)
    cr.set_property('editable', True)

    widget = cr.start_editing(gdk.Event(gdk.NOTHING),
                              gtk.Button(), '',
                              gdk.Rectangle(),
                              gdk.Rectangle(),
                              0)
    assert isinstance(widget, gtk.ComboBoxEntry)

def test_changed_signal_on_render_child():
    '''
    Ensure that ``gtk.CellRendererCombo`` emits the ``changed`` signal
    when its "editing child" emits that signal if that child has an
    active iter.

    :bug: #324282
    '''
    model = gtk.ListStore(str)
    model.append(['hi'])

    cr = gtk.CellRendererCombo()
    cr.set_property('model', model)
    cr.set_property('text-column', 0)
    cr.set_property('editable', True)

    emitted = [0]
    def changed_cb(*args):
        emitted[0] += 1
    cr.connect('changed', changed_cb)

    widget = cr.start_editing(gdk.Event(gdk.NOTHING),
                              gtk.Button(), '',
                              gdk.Rectangle(),
                              gdk.Rectangle(),
                              0)
    widget.set_active(0)
    assert emitted[0] == 1

    # But it is not emitted again if the combo box is inactivated.
    widget.set_active(-1)
    assert emitted[0] == 1

    # But again if it is activated.
    widget.set_active(0)
    assert emitted[0] == 2

def test_set_none_model():
    '''
    Ensure that setting the model property to ``None`` works as
    expected.

    :bug: #526987
    '''
    cr = gtk.CellRendererCombo()
    cr.set_property('model', None)
    assert not cr.get_property('model')
