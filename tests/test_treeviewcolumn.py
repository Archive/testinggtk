'''
Tests for the ``gtk.TreeViewColumn`` class.
'''
import gtk
import utils

def test_default_attributes():
    col = gtk.TreeViewColumn()
    assert col.get_cell_renderers() == []
    assert col.get_spacing() == 0
    assert col.get_sizing() == gtk.TREE_VIEW_COLUMN_GROW_ONLY
    assert col.get_visible()
    assert col.get_width() == 0
    assert col.get_fixed_width() == 1
    assert col.get_min_width() == -1
    assert col.get_max_width() == -1
    assert col.get_sort_column_id() == -1
    assert col.get_sort_order() == gtk.SORT_ASCENDING
    assert col.get_alignment() == 0.0
    assert not col.cell_is_visible()
    assert not col.get_clickable()
    assert not col.get_expand()
    assert not col.get_reorderable()
    assert not col.get_resizable()
    assert not col.get_sort_indicator()
    assert not col.get_title()
    assert not col.get_tree_view()
    assert not col.get_widget()

@utils.pass_on_warnings
def test_cell_get_size():
    '''
    Ensure that a warning is printed when calling ``cell_get_size`` on
    an unrealized ``gtk.TreeViewColumn``.
    '''
    col = gtk.TreeViewColumn()
    col.cell_get_size()

def test_set_get_sizing():
    col = gtk.TreeViewColumn()
    assert col.get_sizing() == gtk.TREE_VIEW_COLUMN_GROW_ONLY

    for sizing in (gtk.TREE_VIEW_COLUMN_FIXED,
                   gtk.TREE_VIEW_COLUMN_AUTOSIZE,
                   gtk.TREE_VIEW_COLUMN_GROW_ONLY):
        col.set_sizing(sizing)
        assert col.get_sizing() == sizing

@utils.pass_on_warnings
def test_cell_set_cell_data_with_incompatible_type():
    '''
    In this test ``gtk.TreeViewColumn.cell_set_cell_data`` is called
    with an iterator that points to a ``gtk.ListStore`` row whose cell
    contains data of type ``object``. When the ``text`` property of
    the ``gtk.CellRendererText`` is assigned with the cells data a
    warning should be shown because type ``PyObject`` cannot be
    converted to ``gchararray``.
    '''
    store = gtk.ListStore(object)
    store.append(['hi'])
    cr = gtk.CellRendererText()
    col = gtk.TreeViewColumn("c1", gtk.CellRendererText(), text = 0)
    iter = store.get_iter((0,))
    col.cell_set_cell_data(store, iter, False, False)

@utils.pass_on_warnings
def test_set_attributes():
    '''
    You get a warning when you try to set a attribute on the renderer
    that does not exist.
    '''
    cr = gtk.CellRendererText()
    c = gtk.TreeViewColumn()
    c.set_attributes(cr, does_not_exist = 33)
