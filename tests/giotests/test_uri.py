'''
Tests for URI handling, parsing, escaping and similar issues.
'''
import gio

def test_path_to_uri():
    file = gio.file_parse_name('/foo/bar')
    assert file.get_uri() == 'file:///foo/bar'
    
