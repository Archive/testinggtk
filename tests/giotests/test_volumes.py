'''
Tests for volume related functionality in gio, such as volume
monitoring, volume management, mount management and drive management.
'''
import gio

def test_monitor_information():
    monitor = gio.volume_monitor_get()
    assert isinstance(monitor.get_connected_drives(), list)
