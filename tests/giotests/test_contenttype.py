'''
Tests for content type functions.
'''
import gio

def test_wrapper_complete():
    '''
    Ensure that all public attributes are wrapped.

    :bug: #539485
    '''
    assert hasattr(gio, 'content_type_guess')
    assert hasattr(gio, 'content_type_from_mime_type')

def test_content_type_guess():
    assert gio.content_type_guess('foo') == 'application/octet-stream'
    assert gio.content_type_guess('.gif') == 'image/gif'
    assert gio.content_type_guess('.jpg') == 'image/jpeg'
    assert gio.content_type_guess('.png') == 'image/png'

def test_content_type_is_a():
    assert gio.content_type_is_a('foo', 'foo')
    assert gio.content_type_is_a('text/html', 'text/plain')
    assert not gio.content_type_is_a('text/plain', 'application/x-tar-gz')
