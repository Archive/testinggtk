'''
Tests for the ``gio.File`` class.
'''
import gio
import os.path

def test_default_attributes():
    file = gio.File('/')
    assert file.get_basename() == '/'
    assert file.get_path() == '/'
    assert file.get_uri() == 'file:///'
    assert file.get_parse_name() == '/'
    assert not file.get_parent()
    assert file.get_uri_scheme() == 'file'

def test_relative_path():
    file = gio.file_parse_name('foo/bar')
    assert file.get_path() == os.path.abspath('foo/bar')
