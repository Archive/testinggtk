'''
Tests for the ``gtk.FileChooserButton`` class.
'''
import gtk

def test_default_attributes():
    fc_button = gtk.FileChooserButton('foo')
    assert fc_button.get_title() == 'foo'
    assert fc_button.get_width_chars() == -1
    assert fc_button.get_focus_on_click()

def test_get_children():
    '''
    Test the types of the filer chooser buttons children.
    '''
    fc_button = gtk.FileChooserButton('foo')
    children = fc_button.get_children()
    assert isinstance(children[0], gtk.Button)
    assert isinstance(children[1], gtk.ComboBox)

def test_default_button_text():
    '''
    Ensure that the default text on the ``gtk.FileChooserButton`` is
    the localized variant of "(None)"
    '''
    fc_button = gtk.FileChooserButton('foo')
    button, combo_box = fc_button.get_children()
    hbox = button.get_children()[0]
    image, label, vsep, image = hbox.get_children()

    # I don't know i18n :)
    assert label.get_text() == "(Ingen)"
