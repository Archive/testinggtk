'''
Tests for the ``gtk.ScrolledWindow`` class.
'''
import gobject
import gtk
import utils

def test_default_adjustments():
    '''
    Ensure that default adjustments are created when a new
    ScrolledWindow is created. 
    '''
    scroll = gtk.ScrolledWindow(None, None)
    assert scroll.get_hadjustment()
    assert scroll.get_vadjustment()

def test_default_scrollbars():
    '''
    Ensure that a ScrolledWindow always has two default scrollbars.
    '''
    scroll = gtk.ScrolledWindow(None, None)
    assert scroll.get_hscrollbar()
    assert scroll.get_vscrollbar()

def test_none_adjustment():
    '''
    Ensure that setting the vertical and horizontal adjustments to
    ``None`` works.

    :bug: #529623
    '''
    scroll = gtk.ScrolledWindow(None, None)
    scroll.set_hadjustment(None)
    scroll.set_vadjustment(None)
    scroll.set_property('hadjustment', None)
    scroll.set_property('vadjustment', None)

@utils.fail_on_warnings
def test_subclass_init_calls_add():
    '''
    Ensure that adding items to a scrolled window within a subclass
    __init__ method works.

    :bug: #438114
    '''
    class MyScrolledWindow(gtk.ScrolledWindow):
        def __init__(self):
            super(MyScrolledWindow, self).__init__()
            self.add(gtk.TextView())
    gobject.type_register(MyScrolledWindow)
    gobject.new(MyScrolledWindow)

def test_scrollbars_in_subclass_init():
    '''
    Ensure that the scrolled windows scrollbars are ``None`` within a
    subclass __init__ method.
    '''
    class Test124(gtk.ScrolledWindow):
        def __init__(self):
            super(Test124, self).__init__()
            assert not self.get_hscrollbar()
            assert not self.get_vscrollbar()
    gobject.type_register(Test124)
    gobject.new(Test124)    

@utils.pass_on_warnings    
def test_add_non_scrollable_widget():
    '''
    Ensure that a warning is printed when you add a non-scrollable
    widget to a ``gtk.ScrolledWindow``.
    '''
    sw = gtk.ScrolledWindow()
    sw.add(gtk.Label('hi'))

def test_show_unfocusable():
    '''
    Ensure that showing an unfocusable scrolled window in a window
    does not provide that scrolled window with focus.
    '''
    scroll = gtk.ScrolledWindow(None, None)
    scroll.set_property('can-focus', False)
    win = gtk.Window()
    win.add(scroll)
    win.show_all()
    assert not win.get_focus()

def test_focus_signal_when_it_has_focus():
    '''
    Ensure a scrolled window will not attempt to regrab focus if it
    already has it.
    '''
    scroll = gtk.ScrolledWindow(None, None)
    win = gtk.Window()
    win.add(scroll)
    scroll.grab_focus()
    assert not scroll.emit('focus', gtk.DIR_UP)

def test_focus_signal_unfocusable():
    '''
    Ensure that a scrolled window will not attempt to grab focus if it
    is unfocusable.

    :bug: #549262
    '''
    scroll = gtk.ScrolledWindow(None, None)
    scroll.set_property('can-focus', False)
    assert not scroll.emit('focus', gtk.DIR_UP)

@utils.fail_on_warnings
def test_destroy_when_scrollbars_are_none():
    '''
    In this test, the scrolled windows scrollbars are NULL when
    destroy() in run. Ensure that gtk.ScrolledWindow deals with that
    scenario properly.

    :bug: #551699
    '''
    class Test(gtk.DrawingArea):
        __gsignals__ = {
            'blah' : (gobject.SIGNAL_RUN_LAST,
                      gobject.TYPE_BOOLEAN,
                      (gobject.TYPE_OBJECT, gobject.TYPE_OBJECT))
            }
    widget = Test()
    Test.set_set_scroll_adjustments_signal('blah')
    sw = gtk.ScrolledWindow()
    sw.add(widget)
    sw.destroy()
