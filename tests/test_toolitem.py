'''
Tests for the ``gtk.ToolItem`` class.
'''
import gtk
import utils

def test_default_attributes():
    ti = gtk.ToolItem()
    assert not ti.get_homogeneous()
    assert not ti.get_expand()
    assert not ti.get_use_drag_window()
    assert not ti.get_is_important()
    assert ti.get_visible_horizontal()
    assert ti.get_visible_vertical()
    assert ti.get_icon_size() == gtk.ICON_SIZE_LARGE_TOOLBAR
    assert ti.get_orientation() == gtk.ORIENTATION_HORIZONTAL
    assert ti.get_toolbar_style() == gtk.TOOLBAR_ICONS
    assert ti.get_relief_style() == gtk.RELIEF_NONE

    # Inherited properties
    assert not ti.get_property('visible')

def test_toolbar_style():
    '''
    Ensure that the toolitem notices changes in its toolbars style.
    '''
    tb = gtk.Toolbar()
    ti = gtk.ToolItem()
    assert ti.get_toolbar_style() == gtk.TOOLBAR_ICONS

    tb.add(ti)
    tb.set_style(gtk.TOOLBAR_BOTH)
    assert ti.get_toolbar_style() == gtk.TOOLBAR_BOTH
    
def test_create_menu_proxy_signal():
    '''
    Ensure that the ``create-menu-proxy`` signal is emitted when
    ``gtk.ToolItem.retrieve_proxy_menu_item`` is called.
    '''
    emitted = [False]
    def create_menu_proxy_cb(toolitem):
        emitted[0] = True
        
    ti = gtk.ToolItem()
    ti.connect('create-menu-proxy', create_menu_proxy_cb)
    ti.retrieve_proxy_menu_item()
    assert emitted[0]

def test_set_get_is_important():
    ti = gtk.ToolItem()
    assert not ti.get_is_important()
    ti.set_is_important(True)
    assert ti.get_is_important()
    ti.set_is_important(False)
    assert not ti.get_is_important()

def test_set_get_proxy_menu_item():
    ti = gtk.ToolItem()
    assert not ti.get_proxy_menu_item('foo')
    mi = gtk.MenuItem()
    ti.set_proxy_menu_item('foo', mi)
    assert ti.get_proxy_menu_item('foo') == mi
    assert not ti.get_proxy_menu_item('bar')
    ti.set_proxy_menu_item('bar', mi)
    assert not ti.get_proxy_menu_item('foo')
    assert ti.get_proxy_menu_item('bar') == mi

#@utils.pass_on_warnings
def test_tool_item_loop():
    '''
    Disabled due to infinite loop.

    :bug: #539807
    '''
    # ti1 = gtk.ToolItem()
    # ti2 = gtk.ToolItem()
    # ti3 = gtk.ToolItem()
    # ti1.add(ti2)
    # ti2.add(ti3)
    # ti3.add(ti1)
