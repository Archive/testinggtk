'''
Tests for the ``gtk.Range`` class.
'''
import gtk
from gtk import gdk

def test_default_properties():
    range = gtk.HScrollbar()
    assert range.get_value() == 0.0
    assert isinstance(range.get_adjustment(), gtk.Adjustment)

def test_scroll_event_signal():
    '''
    Ensure that the ``change-value`` signal is emitted when the range
    receives a ``scroll-event``.

    :bug: #524203
    '''
    def sig_change_value(range, scroll_type, value):
        sig_change_value.called = True
    sig_change_value.called = False
    
    range = gtk.HScrollbar()
    range.connect('change-value', sig_change_value)

    ev = gdk.Event(gdk.SCROLL)
    ev.direction = gdk.SCROLL_DOWN
    gtk.Range.do_scroll_event(range, ev)

    assert sig_change_value.called

def test_set_get_inverted():
    range = gtk.HScrollbar()
    assert not range.get_inverted()
    range.set_inverted(True)
    assert range.get_inverted()
    range.set_inverted(False)
    assert not range.get_inverted()

def test_destroy():
    '''
    Disabled due to segfault
    
    :bug: #551317
    '''
    # range = gtk.HScrollbar()
    # range.destroy()
    # range.set_lower_stepper_sensitivity(gtk.SENSITIVITY_ON)
