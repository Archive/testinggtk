'''
Tests for the ``gtk.Entry`` class.
'''
import gtk

def test_default_attributes():
    '''
    :bug: #83935
    '''
    entry = gtk.Entry()
    assert entry.get_visibility()
    assert entry.get_has_frame()
    assert entry.get_layout()
    assert not entry.get_activates_default()
    assert not entry.get_completion()
    assert not entry.get_inner_border()
    assert not entry.get_cursor_hadjustment()
    assert entry.get_max_length() == 0
    assert entry.get_width_chars() == -1
    assert entry.get_text() == ''
    assert entry.get_alignment() == 0.0

    # Is this offset magic?
    assert entry.get_layout_offsets() == (4, -8)

def test_83935():
    '''
    Test for the invisible char selection algorithm. In the new
    system, we check a number of unicode characters and choose one of
    them which is available in the current font.

    :bug: #83935
    '''
    entry = gtk.Entry()
    ch = entry.get_invisible_char()
    assert ch in [u'\u25cf', u'\u2022', u'\u2731', u'\u273a', u'*']

    # This character should be available in the current font.
    layout = entry.create_pango_layout(ch)
    assert layout.get_unknown_glyphs_count() == 0

def test_delete_text():
    entry = gtk.Entry()
    entry.set_text('Hello, World!')
    entry.delete_text(0, -1)
    assert entry.get_text() == ''

def test_insert_text_signal():
    '''
    Ensure that the ``insert-text`` signal is emitted when the text in
    a ``gtk.Entry`` is changed.
    '''
    emitted = [False]
    def insert_text_cb(*args):
        emitted[0] = True
    entry = gtk.Entry()
    entry.connect('insert-text', insert_text_cb)
    entry.set_text('foo')
    assert emitted[0]

def test_changed_signal():
    '''
    Ensure that the ``changed`` signal is emitted when the text in a
    ``gtk.Entry`` is changed.
    '''
    emitted = [False]
    def changed_cb(*args):
        emitted[0] = True
    entry = gtk.Entry()
    entry.connect('changed', changed_cb)
    entry.insert_text('hello')
    assert emitted[0]

def test_set_max_length_oob():
    entry = gtk.Entry()
    entry.set_max_length(123456789)
    assert entry.get_max_length() == 65535
    entry.set_max_length(-100)
    assert entry.get_max_length() == 0

def test_set_get_invisible_char():
    '''
    Test getting and setting the invisible char property.

    :bug: #83935
    '''
    entry = gtk.Entry()
    ch = entry.get_invisible_char()
    assert ch in [u'\u25cf', u'\u2022', u'\u2731', u'\u273a', u'*']
    entry.set_invisible_char('f')
    assert entry.get_invisible_char() == 'f'
    entry.set_invisible_char('X')
    assert entry.get_invisible_char() == 'X'

def test_long_invisible_char():
    '''
    Ensure that a ``ValueError`` is raised when the argument to
    ``set_invisible_char`` is more than 1 character long.
    '''
    entry = gtk.Entry()
    try:
        entry.set_invisible_char('foo')
        assert False
    except ValueError:
        assert True

def test_short_invisible_char():
    '''
    Ensure that a ``ValueError`` is raised when the argument to
    ``set_invisible_char`` is an empty string.
    '''
    entry = gtk.Entry()
    try:
        entry.set_invisible_char('')
        assert False
    except ValueError:
        assert True
