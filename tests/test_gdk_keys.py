'''
Tests for the ``gdk.keyval_to_unicode`` and ``gdk.unicode_to_keyval``
functions.
'''
from gtk import gdk

latin1range = range(0x20, 0x7e + 1) + range(0xa0, 0xff + 1)

def test_keyval_to_unicode_for_latin1():
    for keyval in latin1range:
        assert gdk.keyval_to_unicode(keyval) == keyval

def test_unicode_to_keyval_for_latin1():
    for unicode in latin1range:
        assert gdk.unicode_to_keyval(unicode) == unicode

def test_keyval_to_unicode_other():
    '''
    Just sample some data points.
    '''
    assert gdk.keyval_to_unicode(0x01bd) == 0x02dd
    assert gdk.keyval_to_unicode(0x06b9) == 0x0409

def test_unicode_to_keyval_other():
    assert gdk.unicode_to_keyval(0x30ab) == 0x04b6
    assert gdk.unicode_to_keyval(0x0652) == 0x05f2
