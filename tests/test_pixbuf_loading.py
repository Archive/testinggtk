'''
Tests for loading ``gdk.Pixbuf`` objects from data.
'''
from gtk import gdk
import utils

@utils.pass_on_warnings
def test_load_from_xpm():
    '''
    Ensure that gdk-pixbuf does not crasch when you try to construct a
    pixbuf from an empty XPM.

    :bug: #467051
    '''
    try:
        xpm = gdk.pixbuf_new_from_xpm_data([])
        assert False
    except IOError:
        assert True
