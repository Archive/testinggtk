'''
Tests for the ``gtk.Plug`` class.
'''
import gtk
from gtk import gdk

def test_default_attributes():
    '''
    This test ensures that no segfault occurs when creating a plug
    with window_id 0.

    :bug: #539363
    '''
    plug = gtk.Plug(0L)
    plug.get_id()

def test_overflowing_socket_id():
    try:
        plug = gtk.Plug(123456789123456789123456789)
        assert False
    except OverflowError:
        assert True

def test_create_with_display():
    plug = gtk.Plug(socket_id = 0L, display = gdk.display_get_default())
    print plug.get_id()
    assert plug.get_id() == 0

def test_create_without_socket_id():
    try:
        gtk.Plug(display = gdk.display_get_default())
        assert False
    except TypeError:
        assert True

def test_create_using_integer():
    '''
    Ensure that it is possible to create a ``gtk.Plug`` object using
    an integer ``socket_id``.

    :bug: #539365
    '''
    plug = gtk.Plug(0)
    assert isinstance(plug, gtk.Plug)
