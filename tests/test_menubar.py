'''
Tests for the ``gtk.MenuBar`` class.
'''
import gtk
from gtk import gdk
import gobject
import utils

def test_default_attributes():
    menubar = gtk.MenuBar()
    assert menubar.get_pack_direction() == gtk.PACK_DIRECTION_LTR
    assert menubar.get_child_pack_direction() == gtk.PACK_DIRECTION_LTR
    assert len(menubar.get_children()) == 0

def test_append_none():
    '''
    Ensure that a ``TypeError`` is raised if ``None`` is appended to a
    ``gtk.MenuBar``.
    '''
    menubar = gtk.MenuBar()
    try:
        menubar.append(None)
        assert False
    except TypeError:
        assert True

@utils.pass_on_warnings        
def test_append_not_menuitem():
    '''
    Ensure that a GtkWarning is displayed if a non-menuitem is
    appended to a ``gtk.MenuBar``.
    '''
    menubar = gtk.MenuBar()
    menubar.append(gtk.Label())

def test_subclass_menubar():
    '''
    Ensure that subclassing the ``gtk.MenuBar`` class works as
    expected.
    '''
    class MyMenuBar(gtk.MenuBar):
        def __init__(self):
            gtk.MenuBar.__init__(self)
    gobject.type_register(MyMenuBar)
    bar = MyMenuBar()
