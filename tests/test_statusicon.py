'''
Tests for the ``gtk.StatusIcon`` class.
'''
import gtk

def test_default_values():
    icon = gtk.StatusIcon()
    assert not icon.get_storage_type()
    assert not icon.get_pixbuf()
    assert not icon.get_stock()
    assert not icon.get_icon_name()

def test_set_stock_icon():
    status_icon = gtk.StatusIcon()
    status_icon.set_from_stock(gtk.STOCK_OPEN)
    assert status_icon.get_stock() == gtk.STOCK_OPEN

def test_set_none_screen():
    '''
    Ensure that TypeError is raised if None is set as the
    gtk.StatusIcon's screen.
    '''
    status_icon = gtk.StatusIcon()
    try:
        status_icon.set_screen(None)
        assert False
    except TypeError:
        assert True
    
