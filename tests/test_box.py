'''
Tests for the ``gtk.Box`` abstract class.
'''
import gtk
import utils

@utils.pass_on_warnings
def test_add_widget_twice():
    '''
    Ensure that you can't add the same widget twice to the same
    ``gtk.Box``.
    '''
    box = gtk.HBox()
    label = gtk.Label('hi')
    box.add(label)
    box.add(label)

#@utils.pass_on_warnings
def test_looping():
    '''
    A ``GtkWarning`` should be printed when you attempt to add an item
    to a container that is already an ancestor of that container.

    Disabled due to infinite loop.

    :bug: #539807
    '''
    # box1 = gtk.HBox()
    # box2 = gtk.HBox()
    # box1.add(box2)
    # box2.add(box1)

@utils.pass_on_warnings
def test_add_box_to_itself():
    '''
    Check that you can't add a ``gtk.HBox`` to itself.
    '''
    box = gtk.HBox()
    box.add(box)

@utils.pass_on_warnings
def test_reorder_not_present_child():
    '''
    Ensure that a warning is printed when reorder_child() is called on
    a widget not present in the gtk.Box.
    '''
    box = gtk.HBox()
    box.reorder_child(gtk.Label(), -1)

def test_reorder_child():
    '''
    Ensure that reorder_child() reorders the widget in the box.
    '''
    box = gtk.HBox()
    w1 = gtk.Button()
    w2 = gtk.Label()
    box.add(w1)
    box.add(w2)

    assert box.get_children() == [w1, w2]
    box.reorder_child(w2, 0)
    assert box.get_children() == [w2, w1]

def test_reorder_child_emits_child_notify():
    '''
    Ensure that ``gtk.Box.reorder_child`` emits the child-notify
    signal for the ``position`` property.
    '''
    box = gtk.HBox()
    w1 = gtk.Button()
    w2 = gtk.Label()
    box.add(w1)
    box.add(w2)

    prop_name = [False]
    def child_notify_cb(widget, param):
        prop_name[0] = param.name
    w2.connect('child-notify', child_notify_cb)

    box.reorder_child(w2, 0)
    assert prop_name[0] == 'position'
