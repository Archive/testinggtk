'''
Tests for the ``gtk.Widget`` class. Since that class is abstract,
subclasses of it is used in these tests.

:group Tooltip Tests: test_set_get_has_tooltip, test_set_tooltip_text,
    test_set_empty_tooltip, test_get_has_tooltip,
    test_tooltip_overlapping_widgets
:group Unparenting Tests: test_unparent,
    test_unparent_with_parent_clears_parent_window,
    test_unparent_without_parent_leaves_parent_window,
    test_unparent_clears_parent_focus_child
:group Focus and Grab Tests: test_set_get_can_focus, test_grab_add,
    test_has_grab_flag, test_grab_insensitive_widget, test_grab_focus,
    test_grab_focus_unfocusable
'''
import atk
import gobject
import gtk
from gtk import gdk
import pango
import utils

def test_wrapper_complete():
    '''
    Ensure that all public attributes are wrapped.
    '''
    assert hasattr(gtk.Widget, 'get_has_tooltip')
    assert hasattr(gtk.Widget, 'set_has_tooltip')

def test_default_attributes():
    '''
    Test that checks the default values in ``gtk.Widget``.
    '''
    widget = gtk.Label()

    # Attributes that are False
    assert not widget.get_has_tooltip()
    assert not widget.get_no_show_all()
    assert not widget.has_screen()
    assert not widget.is_composited()
    assert not widget.is_focus()

    # Attributes that are True
    assert widget.get_child_visible()

    # Attributes that are None
    assert not widget.get_action()
    assert not widget.get_composite_name()
    assert not widget.get_parent_window()
    assert not widget.get_tooltip_markup()
    assert not widget.get_tooltip_text()
    assert not widget.get_tooltip_window()
    assert not widget.get_window()

    # Coordinate and size attributes
    assert widget.get_child_requisition() == (0, 0)
    assert widget.get_pointer() == (-1, -1)
    assert widget.get_size_request() == (-1, -1)

    # List attributes
    assert widget.list_accel_closures() == []
    assert widget.list_mnemonic_labels() == []

    # Enum attributes
    assert widget.get_extension_events() == gdk.EXTENSION_EVENTS_NONE
    assert widget.get_direction() == gtk.TEXT_DIR_LTR

    # Flag attributes
    assert widget.get_events() == 0

    # Objects
    assert isinstance(widget.get_accessible(), atk.NoOpObject)
    assert isinstance(widget.get_colormap(), gdk.Colormap)
    assert isinstance(widget.get_display(), gdk.Display)
    assert isinstance(widget.get_modifier_style(), gtk.RcStyle)
    assert isinstance(widget.get_pango_context(), pango.Context)
    assert isinstance(widget.get_root_window(), gdk.Window)
    assert isinstance(widget.get_screen(), gdk.Screen)
    assert isinstance(widget.get_settings(), gtk.Settings)
    assert isinstance(widget.get_style(), gtk.Style)
    assert widget.get_toplevel() == widget
    assert isinstance(widget.get_visual(), gdk.Visual)

    # Check widget attributes
    assert widget.state == gtk.STATE_NORMAL
    assert widget.allocation
    assert widget.style

    assert not widget.name
    assert not widget.parent
    assert not widget.saved_state
    assert not widget.window

def test_toplevel_in_window():
    '''
    Ensure that the toplevel for a ``gtk.Widget`` is the
    ``gtk.Window`` it is packed in.
    '''
    win = gtk.Window()
    widget = gtk.Button()
    win.add(widget)
    assert widget.get_toplevel() == win

    # The window should be the toplevel even if it is not the widgets
    # direct parent.
    win = gtk.Window()
    widget = gtk.Button()
    box = gtk.HBox()
    box.add(widget)
    win.add(box)
    assert widget.get_toplevel() == win

@utils.pass_on_warnings
def test_realize_not_anchored_widget():
    '''
    Ensure that a warning message is printed if realize() is called on
    a not anchored widget.
    '''
    label = gtk.Label()
    label.realize()

def test_set_child_visible():
    '''
    Ensure that setting and getting the child visible attribute works
    as expected.
    '''
    widget = gtk.Button()
    widget.set_child_visible(False)
    assert not widget.get_child_visible()
    widget.set_child_visible(True)
    assert widget.get_child_visible()

def test_widget_visibility():
    '''
    Ensure that changing the ``visible`` property works as
    expected.
    '''
    widget = gtk.Button()
    assert not widget.get_property('visible')
    widget.show()
    assert widget.get_property('visible')
    widget.hide()
    assert not widget.get_property('visible')



@utils.pass_on_warnings    
def test_realize_without_parent():
    '''
    Ensure that a warning message is printed if an attempt is made to
    realize an unanchored widget.
    '''
    gtk.Button().realize()

def test_get_unknown_style_property():
    '''
    Ensure that attempting to get the value of an unknown style
    property results in a ``TypeError``.
    '''
    b = gtk.Button()
    try:
        b.style_get_property('foobar')
        assert False
    except TypeError:
        assert True

def test_set_get_no_window_flag():
    widget = gtk.Button()
    assert widget.flags() & gtk.NO_WINDOW
    widget.unset_flags(gtk.NO_WINDOW)
    assert (widget.flags() & gtk.NO_WINDOW) == 0
    widget.set_flags(gtk.NO_WINDOW)
    assert widget.flags() & gtk.NO_WINDOW

def test_set_get_parent_window():
    widget = gtk.Button()
    assert not widget.get_parent_window()
    window = gdk.Window(None,
                        100, 100,
                        gdk.WINDOW_TOPLEVEL,
                        0,
                        gdk.INPUT_OUTPUT,
                        x = 100, y = 100)
    widget.set_parent_window(window)
    assert widget.get_parent_window() == window
    widget.set_parent_window(None)
    assert not widget.get_parent_window()

def test_set_none_parent_window():
    '''
    Ensure that ``None`` is accepted as the parent window for the
    widget.

    :bug: #551505
    '''
    widget = gtk.Button()
    widget.set_parent_window(None)

def test_hierarchy_changed_signal():
    '''
    Ensure that the ``hierarchy-signal`` is emitted when the widget
    gets a different parent.
    '''
    data = []
    def hierarchy_cb(widget, prev_top):
        data.append((widget, prev_top))
    widget = gtk.Button()
    widget.connect('hierarchy-changed', hierarchy_cb)
    win = gtk.Window()
    win.add(widget)
    assert data[0] == (widget, None)

def test_get_snapshot_of_unrealized_widget():
    widget = gtk.Button()
    pixmap = widget.get_snapshot(gtk.gdk.Rectangle(0, 0, 100, 100))
    assert not pixmap

def test_get_snapshot_optional_clip_rect():
    '''
    Ensure that the ``clip_rect`` argument to ``get_snapshot`` is
    optional.

    :bug: #548349
    '''
    widget = gtk.Button()
    widget.get_snapshot()

@utils.pass_on_warnings
def test_map_invisible_widget():
    '''
    Ensure that mapping an invisible widget prints a warning message.
    '''
    widget = gtk.Button()
    widget.map()

#@utils.pass_on_warnings
def test_map_visible_widget():
    '''
    Ensure that mapping a visible unrealized widget fails with a
    warning message.

    Disabled due to segfault

    :bug: #551303
    '''
    # widget = gtk.Button('hi')
    # widget.set_flags(gtk.VISIBLE)
    # widget.map()

#@utils.pass_on_warnings
def test_realize_no_window_widget():
    '''
    In this test a ``gtk.NO_WINDOW`` ``gtk.Widget`` that does not
    implement the realized signal handler is realized. A warning
    message should be printed.

    Disabled due to segfault

    :bug: #551307
    '''
    # class Test586(gtk.Widget):
    #    def __init__(self):
    #        super(Test586, self).__init__()
    # gobject.type_register(Test586)
    # 
    # t = Test586()
    # t.unset_flags(gtk.NO_WINDOW)
    # win = gtk.Window()
    # win.add(t)
    # t.realize()

######################################################################
##### Tooltip Tests ##################################################
######################################################################
def test_set_get_has_tooltip():
    widget = gtk.Button()
    assert not widget.get_has_tooltip()
    widget.set_has_tooltip(True)
    assert widget.get_has_tooltip()
    widget.set_has_tooltip(False)
    assert not widget.get_has_tooltip()


def test_set_tooltip_text():
    '''
    Ensure that setting and getting the tooltip text attribute works
    as expected.
    '''
    widget = gtk.Button()
    widget.set_tooltip_text('foo')
    assert widget.get_tooltip_text() == 'foo'
    widget.set_tooltip_text('bar')
    assert widget.get_tooltip_text() == 'bar'

def test_set_empty_tooltip():
    '''
    Setting an empty string as the tooltip for a widget, unsets that
    widgets tooltip.

    :bug: #504456, #541399
    '''
    widget = gtk.Button()
    widget.set_tooltip_text('hello')
    assert widget.get_tooltip_text() == 'hello'
    assert widget.get_has_tooltip()
    widget.set_tooltip_text('')
    assert not widget.get_tooltip_text()
    assert not widget.get_has_tooltip()

def test_get_has_tooltip():
    '''
    Ensure that ``get_has_tooltip`` returns ``True`` if the widget has
    a tooltip, ``False`` otherwise.
    '''
    widget = gtk.Button()
    assert not widget.get_has_tooltip()
    widget.set_tooltip_text('hello')
    assert widget.get_has_tooltip()

def test_tooltip_overlapping_widgets():
    '''
    Ensure that the correct tooltip is shown when two widgets inside a
    ``gtk.Layout`` overlaps each other.

    :bug: #550345
    '''
    b1 = gtk.Button('1')
    b1.set_size_request(100, 100)
    b1.set_tooltip_text('button1')

    b2 = gtk.Button('2')
    b2.set_size_request(100, 100)
    b2.set_tooltip_text('button2')

    # Note that in this layout b2 overlaps b1.
    layout = gtk.Layout()
    layout.set_size_request(400, 400)
    layout.put(b1, 0, 0)
    layout.put(b2, 50, 0)
    win = gtk.Window()
    win.add(layout)
    win.show_all()

    # By setting the timeout to 0 the tooltip should show after one
    # iteration of the mainloop.
    settings = gtk.settings_get_default()
    settings.set_property('gtk-tooltip-timeout', 0)

    # Simulate a motion notify over the second button on a location
    # where it overlaps the first button.
    ev = gdk.Event(gdk.MOTION_NOTIFY)
    ev.window = win.window
    ev.x = ev.x_root = 80.0
    ev.y = ev.y_root = 10.0
    ev.is_hint = False

    # GTK gets the motion notify event and shows the tooltip in
    # response.
    gtk.main_do_event(ev)
    gtk.main_iteration()

    # Find the tooltip window in the toplevel list.
    tooltip_win = None
    for w in gtk.window_list_toplevels():
        if w.name == 'gtk-tooltip':
            tooltip_win = w

    # The tooltip label shown should be the second buttons.
    tooltip_label = tooltip_win.get_child().get_child().get_children()[1]
    assert tooltip_label.get_text() == 'button2'

######################################################################
##### Unparenting tests ##############################################
######################################################################
def test_unparent():
    '''
    Ensure that unparenting a widget clears its parent.
    '''
    parent = gtk.HBox()
    child = gtk.HBox()
    parent.add(child)
    child.unparent()
    assert not child.get_parent()

def test_unparent_with_parent_clears_parent_window():
    '''
    Ensure that unparenting a widget clears its parent window. If the
    widget has a parent.
    '''
    window = gdk.Window(None,
                        100, 100,
                        gdk.WINDOW_TOPLEVEL,
                        0,
                        gdk.INPUT_OUTPUT,
                        x = 100, y = 100)
    widget = gtk.Button()
    parent = gtk.HBox()
    widget.set_parent(parent)
    widget.set_parent_window(window)

    assert widget.get_parent_window()
    widget.unparent()
    assert not widget.get_parent_window()

def test_unparent_without_parent_leaves_parent_window():
    '''
    Ensure that unparenting a widget without a parent leaves its
    parent window attribute untouched.
    '''
    window = gdk.Window(None,
                        100, 100,
                        gdk.WINDOW_TOPLEVEL,
                        0,
                        gdk.INPUT_OUTPUT,
                        x = 100, y = 100)
    widget = gtk.Button()
    widget.set_parent_window(window)
    assert widget.get_parent_window()
    widget.unparent()
    assert widget.get_parent_window()

def test_unparent_clears_parent_focus_child():
    '''
    Ensure that unparenting a widget that is its parents focus child,
    clears that parents focus child attribute.
    '''
    widget = gtk.Button()
    container = gtk.HBox()
    container.add(widget)
    container.set_focus_child(widget)
    widget.unparent()
    assert not container.get_focus_child()
    # This is needed not to provoke #551349
    widget.set_parent(container)

######################################################################
##### Focus and grab tests ###########################################
######################################################################
def test_set_get_can_focus():
    widget = gtk.Button()
    assert widget.get_property('can-focus')
    widget.set_property('can-focus', False)
    assert not widget.get_property('can-focus')
    widget.set_property('can-focus', True)
    assert widget.get_property('can-focus')

def test_grab_add():
    '''
    Ensure that when a widget is made the current grabbing widget,
    then ``gtk.grab_get_current`` returns that widget.
    '''
    widget = gtk.Button()
    widget.grab_add()
    assert gtk.grab_get_current() == widget

def test_has_grab_flag():
    '''
    Ensure that when a widget is grabbed, the ``gtk.HAS_GRAB`` flag is
    set on that flag and unset when the grab is removed.
    '''
    widget = gtk.Button()
    assert not widget.flags() & gtk.HAS_GRAB
    widget.grab_add()
    assert widget.flags() & gtk.HAS_GRAB
    widget.grab_remove()
    assert not widget.flags() & gtk.HAS_GRAB

def test_grab_insensitive_widget():
    '''
    Ensure that an insensitive widget cannot be grabbed.

    :bug: #540834
    '''
    # Clean up previous grab.
    current_grabber = gtk.grab_get_current()
    if current_grabber:
        current_grabber.grab_remove()
    assert not gtk.grab_get_current()
    widget = gtk.Button()
    widget.unset_flags(gtk.SENSITIVE)
    widget.grab_add()
    assert not widget.flags() & gtk.HAS_GRAB
    assert not gtk.grab_get_current()
    widget.grab_remove()

def test_grab_focus():
    '''
    Ensure that calling ``grab_focus`` on a widget makes that widget
    the focus widget within its toplevel.
    '''
    widget = gtk.Button()
    win = gtk.Window()
    win.add(widget)
    widget.grab_focus()
    assert widget.is_focus()

def test_grab_focus_unfocusable():
    '''
    Ensure that calling ``grab_focus`` on an unfocusable widget does
    not have any effect.
    '''
    widget = gtk.Button()
    widget.set_property('can-focus', False)
    win = gtk.Window()
    win.add(widget)
    widget.grab_focus()
    assert not widget.is_focus()

