'''
Tests for the ``gtk.TreeStore`` class.
'''
import gtk
import utils

@utils.fail_on_warnings
def test_insert_in_filtered_tree():
    '''
    Tests what happens when you insert something in a filtered
    ``gtk.TreeStore``.
    '''
    store = gtk.TreeStore(str)
    store.append(None, ['moo'])
    filter = store.filter_new()
    filter.set_visible_func(lambda *a: True)
    store.append(None, ['hi'])

