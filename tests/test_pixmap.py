'''
Tests for the ``gtk.gdk.Pixmap`` class.
'''
from gtk import gdk
import utils

@utils.pass_on_warnings
def test_none_drawable_no_depth():
    '''
    Ensure that a ``RuntimeError`` is raised if ``gtk.gdk.Pixmap`` is
    created without a drawable with default depth.
    '''
    try:
        gdk.Pixmap(None, 100, 100)
        assert False
    except RuntimeError:
        assert True

def test_default_colormap():
    '''
    Ensure that the default colormap is ``None``.
    '''
    pix = gdk.Pixmap(None, 100, 100, depth = 32)
    assert not pix.get_colormap()
