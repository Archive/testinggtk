'''
Tests for the ``gtk.RecentManager`` class.
'''
import gio
import gtk
from gtk import gdk
import os
import utils

def test_default_attributes():
    rm = gtk.RecentManager()
    assert rm.get_limit() == -1

def test_add_full_no_mime_type():
    '''
    Ensure that a ``RuntimeError`` is raised when attempting to add an
    item without a mime_type.
    '''
    rm = gtk.RecentManager()
    try:
        rm.add_full('hi', dict())
        assert False
    except RuntimeError:
        assert True

def test_add_full_no_app_type():
    '''
    Ensure that a ``RuntimeError`` is raised when attempting to add an
    item without an app_type.
    '''
    rm = gtk.RecentManager()
    try:
        rm.add_full('hi', dict(mime_type = 'foo'))
        assert False
    except RuntimeError:
        assert True

def test_add_full_no_app_name():
    '''
    Ensure that a ``RuntimeError`` is raised when attempting to add an
    item without an app_name attribute.
    '''
    rm = gtk.RecentManager()
    try:
        rm.add_full('hi', dict(mime_type = 'foo',
                               app_type = 'foo'))
        assert False
    except RuntimeError:
        assert True

def test_add_full_no_app_exec():
    '''
    Ensure that a ``RuntimeError`` is raised when attempting to add an
    item without an app_exec attribute.
    '''
    rm = gtk.RecentManager()
    try:
        rm.add_full('hi', dict(mime_type = 'foo',
                               app_type = 'foo',
                               app_name = 'gedit'))
        assert False
    except RuntimeError:
        assert True

def test_add_item():
    rm = gtk.RecentManager()
    file = gio.file_parse_name(__file__)
    uri = file.get_uri()
    assert rm.add_full(uri, dict(mime_type = 'foo',
                                 app_type = 'foo',
                                 app_name = 'boo',
                                 app_exec = 'gedit'))
    recent_info = rm.lookup_item(uri)
    assert recent_info.get_uri() == uri

@utils.fail_on_warnings
def test_recent_info_get_icon_unknown_mime():
    '''
    Get the icon for a ``gtk.RecentInfo`` with an unknown mime
    type. The method should return ``None`` and no warning should be
    printed.

    :bug: #539470
    '''
    rm = gtk.RecentManager()
    rm.add_full('file:///foo', dict(mime_type = 'foo',
                                    app_type = 'foo',
                                    app_name = 'boo',
                                    app_exec = 'gedit'))
    recent_info = rm.lookup_item('file:///foo')
    pixbuf = recent_info.get_icon(48)
    assert isinstance(pixbuf, gdk.Pixbuf)
