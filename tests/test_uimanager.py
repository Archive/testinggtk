# -*- coding: utf-8 -*-
'''
Tests for the ``gtk.UIManager`` class.
'''
import gobject
import gtk
import utils

def test_default_attributes():
    ui = gtk.UIManager()
    assert not ui.get_add_tearoffs()
    assert ui.get_action_groups() == []
    assert isinstance(ui.get_accel_group(), gtk.AccelGroup)
    assert ui.get_ui() == '<ui>\n</ui>\n'

def test_menu_outside_menubar_el():
    '''
    Ensure that a ``GError`` is thrown if a menu tag appears outside a
    menubar tag.
    '''
    xml = '''
    <ui>
        <menu name = "FileMenu" action = "FileMenuAction"/>
    </ui>'''
    ui = gtk.UIManager()
    try:
        ui.add_ui_from_string(xml)
        assert False
    except gobject.GError:
        assert True

@utils.pass_on_warnings
def test_missing_action_in_menu():
    '''
    Ensure that a ``GtkWarning`` is printed if the UI string for a
    menu refers to an action that does not exist in the
    ``gtk.UIManager``.
    '''
    xml = '''
    <ui>
        <menubar>
            <menu name = "FileMenu" action = "FileMenuAction"/>
        </menubar>
    </ui>        
    '''
    ui = gtk.UIManager()
    ui.add_ui_from_string(xml)
    ui.get_widget('/ui/menubar')

@utils.pass_on_warnings
def test_no_action_in_menu():
    '''
    Ensure that a ``GtkWarning`` is printed if the UI string for a
    menu does not have an action attribute.
    '''
    xml = '''
    <ui>
        <menubar>
            <menu name = "FileMenu"/>
        </menubar>
    </ui>        
    '''
    ui = gtk.UIManager()
    ui.add_ui_from_string(xml)
    ui.get_widget('/ui/menubar')

@utils.fail_on_warnings
def test_select_first_in_menubar():
    '''
    Attempt to select the first item in a ``GtkMenubar`` constructed
    using a ``gtk.UIManager``. Test passes if no warnings are printed.

    :bug: #540618
    '''
    xml = '''
    <ui>
        <menubar>
            <menu name = "FileMenu" action = "action"/>
        </menubar>
    </ui>        
    '''
    action = gtk.Action('action', '', '', None)
    action.set_property('hide-if-empty', False)

    ag = gtk.ActionGroup('default')
    ag.add_action(action)

    ui = gtk.UIManager()
    ui.insert_action_group(ag)
    ui.add_ui_from_string(xml)

    menubar = ui.get_widget('/ui/menubar')
    menubar.select_first(False)

@utils.fail_on_warnings
def test_menubar_select_item():
    '''
    Ensure that no warning is shown if an item is selected from a menu
    bar constructed using a ui manager when that items
    ``hide-if-empty`` property is ``False``.

    :bug: #540618
    '''
    xml = '''
    <ui>
        <menubar>
            <menu name = "Menu1" action = "action"/>
            <menu name = "Menu2" action = "action"/>
            <menu name = "Menu3" action = "action"/>
            <menu name = "Menu4" action = "action"/>
            <menu name = "Menu5" action = "action"/>
        </menubar>
    </ui>        
    '''
    action = gtk.Action('action', '', '', None)
    action.set_property('hide-if-empty', False)

    ag = gtk.ActionGroup('default')
    ag.add_action(action)

    ui = gtk.UIManager()
    ui.insert_action_group(ag)
    ui.add_ui_from_string(xml)

    menubar = ui.get_widget('/ui/menubar')
    menubar.select_item(menubar.get_children()[3])

def test_change_hide_if_empty_updates_visibility():
    '''
    Ensure that when the ``hide-if-empty`` property is updated the
    visibility of the associated ``gtk.MenuItem`` is also updated.

    :bug: #540622
    '''
    xml = '''
    <ui>
        <menubar>
            <menu name = "FileMenu" action = "action">
                <menu name = "foo" action = "action"/>
            </menu>
        </menubar>
    </ui>        
    '''
    action = gtk.Action('action', 'name', '', None)
    ag = gtk.ActionGroup('default')
    ag.add_action(action)

    ui = gtk.UIManager()
    ui.insert_action_group(ag)
    ui.add_ui_from_string(xml)

    foomenuitem = ui.get_widget('/ui/menubar/FileMenu/foo')

    # hide-if-empty is True so the menu item is not visible
    assert not foomenuitem.get_property('visible')

    # menu item should now be visible
    action.set_property('hide-if-empty', False)
    assert foomenuitem.get_property('visible')

def test_empty_submenu_hide_if_empty_false():
    '''
    Ensure that an empty submenu whose action has the
    ``hide-if-empty`` property ``False`` has a single item with the
    label "Empty" in its menu.
    '''
    xml = '''
    <ui>
        <menubar>
            <menu name = "FileMenu" action = "action">
                <menu name = "foo" action = "action"/>
            </menu>
        </menubar>
    </ui>        
    '''
    action = gtk.Action('action', 'name', '', None)
    action.set_property('hide-if-empty', False)
    ag = gtk.ActionGroup('default')
    ag.add_action(action)

    ui = gtk.UIManager()
    ui.insert_action_group(ag)
    ui.add_ui_from_string(xml)

    foomenuitem = ui.get_widget('/ui/menubar/FileMenu/foo')
    assert foomenuitem.get_property('visible')

    foomenu = foomenuitem.get_submenu()
    children = foomenu.get_children()
    assert len(children) == 2

    label = children[1].get_child()
    # I don't know i18n :)
    assert label.get_text() == 'Tom'

def test_add_widget_signal():
    '''
    Ensure that the ``add-widget`` signal is emitted when
    ``get_widget`` is called.
    '''
    xml = '''
    <ui>
        <menubar/>
    </ui>
    '''
    data = []
    def add_widget_cb(uimanager, widget):
        data.append((uimanager, widget))
    ui = gtk.UIManager()
    ui.connect('add-widget', add_widget_cb)
    ui.add_ui_from_string(xml)
    menubar = ui.get_widget('/ui/menubar')
    assert data[0][0] == ui
    assert data[0][1] == menubar

def test_action_group_list():
    '''
    Ensure that getting the action groups in a ui manager works as
    expected.
    '''
    groups = [gtk.ActionGroup('%s' % x) for x in range(10)]
    ui = gtk.UIManager()
    for group in groups:
        ui.insert_action_group(group)
    assert groups == ui.get_action_groups()

@utils.pass_on_warnings    
def test_insert_action_group_with_same_name():
    '''
    Ensure that a warning is printed if an action group with the same
    name as an already inserted action group is inserted.
    '''
    ui = gtk.UIManager()
    ui.insert_action_group(gtk.ActionGroup('foo'))
    ui.insert_action_group(gtk.ActionGroup('foo'))

@utils.pass_on_warnings
def test_remove_invalid_action_group():
    '''
    Ensure that a warning is printed if an action group that does not
    exist in the ``gtk.UIManager`` is removed.
    '''
    ui = gtk.UIManager()
    ag = gtk.ActionGroup('foo')
    ui.remove_action_group(ag)

def test_remove_used_action_group():
    '''
    Ensure that removing an action group that is referenced from the
    GUI description works as expected.
    '''
    xml = '''
    <ui>
        <menubar>
            <menu name = "file" action = "file">
                <menu name = "open" action = "open"/>
            </menu>
        </menubar>
    </ui>
    '''
    ag = gtk.ActionGroup('foo')
    ag.add_action(gtk.Action('file', 'name', '', None))
    ag.add_action(gtk.Action('open', 'name', '', None))

    ui = gtk.UIManager()
    ui.insert_action_group(ag)
    ui.add_ui_from_string(xml)
    ui.remove_action_group(ag)
    assert ui.get_action_groups() == []

def test_sync_sensitive():
    '''
    A ``gtk.ToolItem`` is created using a ``gtk.UIManager``. When the
    sensitivity of the action associated with the tool item is
    updated, the sensitivity of the tool item should be updated too.
    '''
    xml = '''
    <ui>
        <toolbar>
            <toolitem name = "ti" action = "ti_action"/>
        </toolbar>
    </ui>
    '''
    ti_action = gtk.Action('ti_action', 'name', '', None)
    ag = gtk.ActionGroup('foo')
    ag.add_action(ti_action)

    ui = gtk.UIManager()
    ui.insert_action_group(ag)
    ui.add_ui_from_string(xml)

    ti = ui.get_widget('/ui/toolbar/ti')
    assert ti.get_property('sensitive')

    for value in (False, True):
        ti_action.set_sensitive(value)
        assert ti.get_property('sensitive') == value

def test_sync_visible():
    '''
    A ``gtk.ToolItem`` is created using a ``gtk.UIManager``. When the
    visibility of the action associated with the tool item is updated,
    the visibility of the tool item should be changed too.
    '''
    xml = '''
    <ui>
        <toolbar>
            <toolitem name = "ti" action = "ti_action"/>
        </toolbar>
    </ui>
    '''
    ti_action = gtk.Action('ti_action', 'name', '', None)
    ag = gtk.ActionGroup('foo')
    ag.add_action(ti_action)

    ui = gtk.UIManager()
    ui.insert_action_group(ag)
    ui.add_ui_from_string(xml)

    ti = ui.get_widget('/ui/toolbar/ti')
    assert ti.get_property('visible')

    for value in (False, True):
        ti_action.set_visible(value)
        assert ti.get_property('visible') == value
