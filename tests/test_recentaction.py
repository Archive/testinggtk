'''
Tests for the ``gtk.RecentAction`` class.
'''
import gtk

def test_none_stock_id():
    '''
    Ensure that a ``gtk.RecentAction`` can be created with ``None`` as
    the stock_id.

    :bug: #536882
    '''
    gtk.RecentAction('name', 'label', 'tooltip', None)

def test_default_attributes():
    action = gtk.RecentAction('name', 'label', 'tooltip', 'stock-id')
    assert not action.get_show_numbers()
