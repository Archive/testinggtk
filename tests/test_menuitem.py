'''
Tests for the ``gtk.MenuItem`` class.
'''
import gtk
import utils

def test_default_attributes():
    mi = gtk.MenuItem()
    assert not mi.get_submenu()
    assert not mi.get_right_justified()
    assert not mi.get_child()

def test_menu_item_with_label():
    '''
    Ensure that a ``gtk.MenuItem`` with a label has a
    ``gtk.AccelLabel`` as the child.
    '''
    mi = gtk.MenuItem(label = 'hi')
    assert isinstance(mi.get_child(), gtk.AccelLabel)

@utils.fail_on_warnings
def test_set_non_submenu():
    '''
    Ensure that a TypeError is raised when you attempt to set a non
    ``gtk.Menu`` widget as the submenu.

    :bug: #534675
    '''
    mi = gtk.MenuItem()
    try:
        mi.set_submenu(gtk.Label('hi'))
        assert False
    except TypeError:
        assert True

def test_set_non_submenu_property():
    '''
    Ensure that a ``TypeError`` is raised when you attempt to set a
    non ``gtk.Menu`` widget as the submenu using the submenu property.
    '''
    mi = gtk.MenuItem()
    try:
        mi.set_property('submenu', gtk.Label('hi'))
        assert False
    except TypeError:
        assert True

def test_set_get_submenu():
    '''
    Ensure that it is possible to set and get the submenu attribute.

    :bug: #534675
    '''
    mi = gtk.MenuItem()
    assert not mi.get_submenu()
    menu = gtk.Menu()
    mi.set_submenu(menu)
    assert mi.get_submenu() == menu
    mi.set_submenu(None)
    assert not mi.get_submenu()

#@utils.pass_on_warnings
def test_menuitem_looping():
    '''
    Disabled due to infinite loop.

    :bug: #539807
    '''
    # mi1 = gtk.MenuItem()
    # mi2 = gtk.MenuItem()
    # mi3 = gtk.MenuItem()
    # mi1.add(mi2)
    # mi2.add(mi3)
    # mi3.add(mi1)

def test_toggle_size_request_signal():
    '''
    Ensure that calling ``toggle_size_request`` emits the
    ``toggle-size-request`` signal and that the correct size request
    value is returned.
    '''
    emitted = [False]
    def cb(*args):
        emitted[0] = True
    mi = gtk.MenuItem()
    mi.connect('toggle-size-request', cb)
    assert mi.toggle_size_request() == 0
    assert emitted[0]

@utils.fail_on_warnings
def test_select_menu_item_with_submenu():
    '''
    Selecting a submenu on a menu item without a parent should work as
    expected and has no effect.

    :bug: #541315
    '''
    menu = gtk.Menu()
    mi = gtk.MenuItem('hi')
    mi.set_submenu(menu)
    current_grab = gtk.grab_get_current()
    mi.select()
    mi.select()
    assert gtk.grab_get_current() == current_grab

