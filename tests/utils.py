'''
Useful utils usable under unit testing. 
'''
import gtk
from gtk import gdk
import StringIO
import sys
import time

class WarningChecker:
    def __init__(self):
        self.stderr = sys.stderr
        self.file = StringIO.StringIO()

    def get_warning_triplets(self):
        lines = self.file.getvalue().split('\n')
        self.file = StringIO.StringIO()
        return zip(lines, lines[1:], lines[2:])
        
    def push_stderr(self):
        sys.stderr = self.file

    def pop_stderr(self):
        sys.stderr = self.stderr

    def pop_stderr_and_fail_warnings(self):
        sys.stderr = self.stderr
        for triplet in self.get_warning_triplets():
            if 'Warning' in triplet[0]:
                raise AssertionError, '\n'.join(triplet)

    def pop_stderr_and_pass_warnings(self):
        sys.stderr = self.stderr
        for triplet in self.get_warning_triplets():
            if 'Warning' in triplet[0]:
                return
        raise AssertionError, 'No Warning from test expecting one!'

wc = WarningChecker()
def pass_on_warnings(func):
    '''
    A decorator that fails the decorated test unless atleast one
    Warning occured during the test run.

    .. python::

       @utils.pass_on_warnings
       def test_something():
           code that emits gtk warnings...

    :param func: test function to decorate               
    '''
    def wrapper():
        wc.push_stderr()
        func()
        wc.pop_stderr_and_pass_warnings()
    wrapper.__module__ = func.__module__
    wrapper.__name__ = func.__name__
    return wrapper

def fail_on_warnings(func):
    '''
    A decorator that fails the decorated test if one or more Warnings
    occured during the test run.

    .. python::
    
       @utils.fail_on_warnings
       def test_something():
           code that shouldn't emit gtk warnings

    :param func: test function to decorate
    '''
    def wrapper():
        wc.push_stderr()
        func()
        wc.pop_stderr_and_fail_warnings()
    wrapper.__module__ = func.__module__
    wrapper.__name__ = func.__name__
    return wrapper

def swallow_warnings(func):
    '''
    A decorator that swallows any printouts on stderr.

    .. python::

       @utils.swallow_warnings
       def test_blaha():
           code that makes warnings...

    :param func: test function to decorate           
    '''
    def wrapper():
        wc.push_stderr()
        func()
        wc.pop_stderr()
    wrapper.__module__ = func.__module__
    wrapper.__name__ = func.__name__
    return wrapper

def gtk_process_all_pending_events():
    '''
    Let GTK process all events in its pending queue without blocking.
    '''
    gtk.main_iteration(False)
    time.sleep(0.05)
    while gtk.events_pending():
        gtk.main_iteration(False)
        time.sleep(0.05)

def gtk_container_find_child(container, path):
    '''
    Returns the widget child in the `container` found when searching
    for `path`.

    .. python::

       # This example finds the page range entry in a PrintUnixDialog.
       dialog = gtkunixprint.PrintUnixDialog()
       path = 'VBox.VBox.Notebook.VBox.HBox.VBox.Alignment.Table.Entry'
       widget = utils.gtk_container_find_child(dialog, path.split('.'))

    :param container: a ``gtk.Container`` to search
    :param path: a list of class names which is the path to the
        widget.
    :return: the ``gtk.Widget`` found or ``None`` if there is no
        widget at the specified path.
    '''
    if not path:
        return container
    for child in container.get_children():
        if child.__class__.__name__ == path[0]:
            return gtk_container_find_child(child, path[1:])
    return None

def gtk_container_print_tree(widget, indent = 0):
    '''
    Debug function that prints a containers widget hierarchy.
    '''
    print ' ' * indent + widget.__class__.__name__
    if not isinstance(widget, gtk.Container):
        return
    for child in widget.get_children():
        gtk_container_print_tree(child, indent + 2)

def pixbuf_count_pixels(pixbuf, rgb):
    '''
    Returns the number of times the pixel `rgb` is present in the
    pixbuf.

    :param pixbuf: a ``gtk.gdk.Pixbuf``
    :param rgb: a color specification string such as "#ff0000"
    :return: the number of times `rgb` appears in the pixbuf
    '''
    # not handling transparent pixbufs yet
    if pixbuf.get_has_alpha():
        raise ValueError('pixbuf must not have alpha')
    col = gdk.color_parse(rgb)

    col.red /= 256
    col.green /= 256
    col.blue /= 256
    red = chr(col.red)
    green = chr(col.green)
    blue = chr(col.blue)

    # Abusing an iterator...
    count = 0
    pixels = iter(pixbuf.get_pixels())
    for r, g, b in zip(pixels, pixels, pixels):
        if r == red and g == green and b == blue:
            count += 1
    return count

def widget_get_rendering(widget):
    '''
    Returns a ``gdk.Pixbuf`` which contains a rendering of the
    `widget` aquired using ``widget.get_snapshot``

    :param widget: a realized ``gtk.Widget``
    :return: a ``gdk.Pixbuf`` of the rendered widget
    '''
    if not widget.window:
        raise ValueError('widget must be realized')
    width, height = widget.window.get_size()
    clip = gdk.Rectangle(0, 0, width, height)
    pixmap = widget.get_snapshot(clip)
    pixbuf = gdk.Pixbuf(gdk.COLORSPACE_RGB, False, 8, width, height)
    pixbuf.get_from_drawable(pixmap, pixmap.get_colormap(),
                             0, 0, 0, 0,
                             width, height)
    return pixbuf
