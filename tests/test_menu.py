'''
Tests for the ``gtk.Menu`` class.
'''
import gtk
from gtk import gdk
import utils

def test_default_attributes():
    menu = gtk.Menu()
    assert not menu.get_active()
    assert not menu.get_accel_group()
    assert not menu.get_attach_widget()
    assert not menu.get_tearoff_state()
    assert not menu.get_title()

def test_appending_menu_items():
    menu = gtk.Menu()
    menu.append(gtk.MenuItem())
    menu.append(gtk.MenuItem())
    assert len(menu.get_children()) == 2

def test_set_get_active():
    menu = gtk.Menu()

    items = [gtk.MenuItem(label = 'hi') for x in range(10)]
    for item in items:
        menu.append(item)

    for idx, item in enumerate(items):
        menu.set_active(idx)
        assert menu.get_active() == item

def test_set_active_on_menu_item_without_child():
    '''
    Ensure that it is possible to set the active menu item to a menu
    item without a child.

    :bug: #539512
    '''
    # Default constructor creates a menu item without a child.
    mi = gtk.MenuItem()
    menu = gtk.Menu()
    menu.append(mi)
    menu.set_active(0)
    assert menu.get_active() == 0

@utils.pass_on_warnings    
def test_add_menu_to_menu():
    menu = gtk.Menu()
    menu.add(menu)

@utils.pass_on_warnings
def test_add_non_menuitem():
    '''
    A ``GtkWarning`` is printed when you add a non ``gtk.MenuItem`` to
    a ``gtk.Menu``.
    '''
    menu = gtk.Menu()
    menu.add(gtk.Label('hi'))

def test_popup():
    '''
    Ensure that ``popup`` shows and grabs the menu widget.
    '''
    menu = gtk.Menu()
    menu.popup(None, None, None, gdk.BUTTON_PRESS, 0, 0)
    assert gtk.grab_get_current() == menu
    assert menu.flags() & gtk.VISIBLE
