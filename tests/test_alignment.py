'''
Tests for the gtk.Alignment class.
'''
import gtk

def test_default_attributes():
    a = gtk.Alignment()
    top, bottom, left, right = a.get_padding()
    assert top == 0.0
    assert bottom == 0.0
    assert left == 0.0
    assert right == 0.0
