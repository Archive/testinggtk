'''
Tests for the gtk.EntryCompletion class.
'''
import gtk
import utils

def test_default_attributes():
    ec = gtk.EntryCompletion()
    assert ec.get_popup_set_width()
    assert ec.get_popup_completion()
    assert ec.get_popup_single_match()
    assert not ec.get_entry()
    assert not ec.get_model()
    assert not ec.get_inline_completion()
    assert not ec.get_inline_selection()
    assert ec.get_text_column() == -1
    assert ec.get_minimum_key_length() == 1

def test_insert_action_text():
    ec = gtk.EntryCompletion()
    for x in range(10):
        ec.insert_action_text(0, 'hi %d' % x)

@utils.pass_on_warnings
def test_delete_action_oob():
    '''
    Ensure that a ``GtkWarning`` is printed when the action index to
    delete is greater than the number of actions in the
    ``gtk.EntryCompletion``.
    '''
    ec = gtk.EntryCompletion()
    ec.delete_action(333)

def test_complete():
    '''
    Ensure that requesting the ``gtk.EntryCompletion`` to complete
    works as expected.

    Disabled due to segfault.

    :bug: #539565
    '''
    # store = gtk.ListStore(str)
    # completion = gtk.EntryCompletion()
    # completion.set_model(store)
    # completion.complete()

@utils.pass_on_warnings
def test_complete_with_text_column_oob():
    '''
    Ensure that a ``GtkWarning`` is printed when completing with a
    text-column that is out of bounds.
    '''
    store = gtk.ListStore(str)
    store.append(['foo'])

    completion = gtk.EntryCompletion()
    completion.set_text_column(99)
    completion.set_model(store)
    entry = gtk.Entry()
    entry.set_completion(completion)
    completion.complete()
