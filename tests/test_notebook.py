'''
Tests for the ``gtk.Notebook`` class.
'''
import gtk
from gtk import gdk

def test_default_attributes():
    '''
    Test that checks the default values in ``gtk.Notebook``.
    '''
    book = gtk.Notebook()
    assert book.get_current_page() == -1
    assert book.get_group_id() == -1
    assert book.get_n_pages() == 0
    assert book.get_show_border()
    assert book.get_show_tabs()
    assert book.get_tab_pos() == gtk.POS_TOP
    assert not book.get_scrollable()
    assert not book.get_nth_page(33)

def test_get_current_page():
    '''
    Ensure that when ``gtk.Notebook`` has one visible page, then
    current page index is 0.

    Note that the child widget has to be shown first, otherwise
    ``gtk.Notebook`` refuses to switch to it. That is specified
    behaviour.

    :bug: #351112, #533742
    '''
    book = gtk.Notebook()
    child = gtk.Button('moo')
    child.show()
    tab_label = gtk.Label('label')
    book.append_page(child, tab_label)
    assert book.get_current_page() == 0

def test_add_invisible_page():
    '''
    Ensure that when adding an invisible page to a ``gtk.Notebook``
    the current page attribute is not updated.

    :bug: #351112, #533742
    '''
    book = gtk.Notebook()
    assert book.get_current_page() == -1
    child = gtk.Button('moo')
    book.append_page(child)
    assert book.get_current_page() == -1

def test_switch_page_signal_emitted():
    '''
    Ensure that the ``switch-page`` signal is emitted when you remove
    the active page in a ``gtk.Notebook``.

    Note that the page widget has to be shown for the notebook to be
    able to switch away from it.

    :bug: #351112, #533742
    '''
    emitted = [False]
    def switch_page_cb(notebook, page, page_num):
        emitted[0] = True

    book = gtk.Notebook()
    for x in range(2):
        page = gtk.Label('page-%d' % x)
        page.show()
        book.append_page(page)
    book.connect('switch-page', switch_page_cb)
    book.remove_page(0)
    assert book.get_current_page() == 0
    assert emitted[0]

def test_switch_page_signal_remove_leftmost_tab():
    '''
    Ensure that when you remove the leftmost tab in a
    ``gtk.Notebook``, the ``switch-page`` signal is emitted with
    ``page_num`` 0.

    Note that the page widget has to be shown for the notebook to be
    able to switch away from it.

    :bug: #131920
    '''
    num_in_cb = ['other']
    def switch_page_cb(notebook, page, page_num):
        num_in_cb[0] = page_num

    book = gtk.Notebook()
    for x in range(5):
        page = gtk.Label('page-%d' % x)
        page.show()
        book.append_page(page, gtk.Label('page-%d' % x))
    book.connect('switch-page', switch_page_cb)
    book.remove_page(0)

    assert num_in_cb[0] == 0

def test_same_window_as_parent():
    '''
    A realized ``gtk.Notebook`` shares the same ``gdk.Window`` as its
    parent.
    '''
    notebook = gtk.Notebook()
    win = gtk.Window()
    win.add(notebook)
    win.show_all()
    assert notebook.window and win.window
    assert notebook.window is win.window

def test_remove_current_page():
    '''
    Ensure that page index is decreased by one when the current page
    is removed, unless it is 0.
    '''
    book = gtk.Notebook()
    for x in range(5):
        page = gtk.Label()
        page.show()
        book.append_page(page, gtk.Label())

    for x in range(4, 0, -1):
        book.set_current_page(x)
        book.remove_page(x)
        assert book.get_current_page() == x - 1

def test_next_page_when_current_removed():
    '''
    Ensure that when the current page is removed, the page with index
    one step lower becomes the new active page.
    '''
    book = gtk.Notebook()
    pages = [gtk.Label() for x in range(5)]
    for page in pages:
        page.show()
        book.append_page(page, gtk.Label())

    for x in range(4, 0, -1):
        book.set_current_page(x)
        book.remove_page(x)
        page = book.get_nth_page(book.get_current_page())
        assert page == pages[x - 1]

def test_press_arrow_on_not_can_focus_notebook():
    '''
    The scroll arrows in a ``gtk.Notebook`` should be clickable and
    should change the current page even if the notebook does not
    accept keyboard focus.

    :bug: #528091
    '''
    book = gtk.Notebook()

    # Show scroll arrows
    book.set_scrollable(True)

    # Unfocusable
    book.set_property('can-focus', False)

    # Add four dummy pages
    for page in range(4):
        page = gtk.Label('A')
        book.append_page(page, gtk.Label('A'))

    win = gtk.Window()
    win.add(book)
    win.show_all()

    book.set_current_page(3)

    # This will simulate a left button click on the left notebook
    # arrow. Note that the coordinates are hardcoded, they should be
    # calculated somehow.
    ev = gdk.Event(gdk.BUTTON_PRESS)
    ev.button = 1
    ev.x = 5.0
    ev.y = 5.0
    ev.window = book.window
    book.do_button_press_event(book, ev)

    # Release the button
    ev = gdk.Event(gdk.BUTTON_RELEASE)
    ev.button = 1
    book.do_button_release_event(book, ev)

    assert book.get_current_page() == 2

def test_hide_all_show_all():
    '''
    Ensure that the currently shown page does not change when you
    first call ``hide_all`` and then ``show_all`` on the
    ``gtk.Notebook``.

    :bug: #438318
    '''
    book = gtk.Notebook()
    for x in range(3):
        page = gtk.Label('a')
        page.show()
        book.append_page(page)
    book.show_all()

    book.set_current_page(1)
    book.hide_all()
    book.show_all()
    assert book.get_current_page() == 1
