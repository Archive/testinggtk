'''
Tests for the ``gtk.ToolButton`` class.
'''
import gobject
import gtk

def test_empty_toolbutton():
    tb = gtk.ToolButton()
    assert not tb.get_label()
    assert not tb.get_stock_id()
    assert not tb.get_icon_name()
    assert not tb.get_icon_widget()
    assert not tb.get_label_widget()
    assert not tb.get_use_underline()

class Sub(gtk.ToolButton):
    __gtype_name__ = 'Sub'
    def __init__(self):
        super(Sub, self).__init__()
        self.called = False

    def do_create_menu_proxy(self):
        self.called = True
gobject.type_register(Sub)

def test_create_menu_proxy_signal_on_subclass():
    '''
    Ensure that a subclass of ``gtk.ToolButton`` receives the
    ``create-menu-proxy`` signal.
    '''
    sub = Sub()
    sub.retrieve_proxy_menu_item()
    assert sub.called

