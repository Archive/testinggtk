'''
Tests for the ``gtk.DrawingArea`` class.
'''
import gtk

def test_default_attributes():
    da = gtk.DrawingArea()
    assert (da.flags() & gtk.NO_WINDOW) == 0

def test_no_window_flag():
    '''
    If the ``gtk.NO_WINDOW`` flag is set, then the ``gtk.DrawingArea``
    will use its parents window, otherwise it will use its own window.

    :bug: #519317
    '''
    da = gtk.DrawingArea()
    da.set_flags(gtk.NO_WINDOW)

    win = gtk.Window()
    win.add(da)
    da.realize()
    assert da.window == win.window

    win.remove(da)
    da = gtk.DrawingArea()
    win.add(da)
    da.realize()
    assert da.window != win.window
    
