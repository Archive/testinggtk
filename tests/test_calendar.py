'''
Tests for the ``gtk.Calendar`` class.
'''
import gtk
from datetime import date
import utils

def test_calendar_year_overflowing():
    '''
    Ensure that the Calendar handles years that overflow in a correct
    fashion.

    :bug: #357807
    '''
    year = 9999999
    cal = gtk.Calendar()
    cal.select_month(10, year)
    cal_year, cal_month, cal_date = cal.get_date()
    assert cal_year == year

def test_calendar_year_underflowing():
    '''
    Ensure that the Calendar handles years that underflow in a correct
    fashion.

    :bug: #357807
    '''
    cal = gtk.Calendar()
    cal.select_month(10, -99999999)
    year, month, date = cal.get_date()
    assert year == -99999999

def test_calendar_current_date():
    cal = gtk.Calendar()
    year, month, date_ = cal.get_date()

    d = date.today()

    assert year == d.year
    assert month + 1 == d.month
    assert date_ == d.day

@utils.pass_on_warnings
def test_set_to_big_day():
    cal = gtk.Calendar()
    cal.select_month(0, 2000)
    cal.select_day(39)
    
    
