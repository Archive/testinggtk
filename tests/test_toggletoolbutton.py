'''
Tests for the ``gtk.ToggleToolButton class``.
'''
import gtk

def test_default_attributes():
    ttb = gtk.ToggleToolButton()
    assert not ttb.get_active()
