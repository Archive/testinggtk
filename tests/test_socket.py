'''
Tests for the ``gtk.Socket`` class.
'''
import gtk
from tests import utils

@utils.fail_on_warnings
def test_default_attributes():
    socket = gtk.Socket()
    assert socket.get_id() == 0

@utils.fail_on_warnings
def test_realize():
    '''
    Realizing a ``gtk.Socket`` should set a flag in its flags.
    '''
    win = gtk.Window()
    socket = gtk.Socket()
    win.add(socket)
    socket.realize()
    assert bool(socket.flags() & gtk.REALIZED)
    
