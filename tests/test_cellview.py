'''
Tests for the ``gtk.CellView`` class.
'''
import gtk
from gtk import gdk
import utils

def test_default_attributes():
    cv = gtk.CellView()
    assert not cv.get_property('model')
    assert not cv.get_displayed_row()

def test_new_with_text():
    '''
    Ensure that a ``gtk.CellView`` instantiated using
    ``gtk.cell_view_new_with_text`` has one ``gtk.CellRendererText``
    renderer with the correct properties.
    '''
    store = gtk.ListStore(int)
    store.append([3])
    cv = gtk.cell_view_new_with_text('foo')
    cv.set_model(store)
    cv.set_displayed_row((0,))

    renderers = cv.get_cell_renderers()
    assert len(renderers) == 1
    cr_text = renderers[0]
    assert isinstance(cr_text, gtk.CellRendererText)
    assert cr_text.get_property('text') == 'foo'

@utils.fail_on_warnings    
def test_get_default_cell_renderers():
    '''
    Ensure that ``gtk.CellView`` has an empty list of cell renderers
    by default.

    :bug: #539469
    '''
    cv = gtk.CellView()
    assert cv.get_cell_renderers() == []

def test_adding_cell_renderers():
    '''
    Ensure that cell renderers can be added to a ``gtk.CellView``.
    '''
    store = gtk.ListStore(int)
    store.append([3])
    cv = gtk.CellView()
    cv.set_model(store)
    cv.set_displayed_row((0,))
    for x in range(10):
        cv.pack_start(gtk.CellRendererText())
        assert len(cv.get_cell_renderers()) == x + 1

def test_set_get_model():
    '''
    Ensure that setting and getting the model attribute works as
    expected.

    :bug: #539464
    '''
    cv = gtk.CellView()
    assert not cv.get_model()
    store = gtk.ListStore(int)
    cv.set_model(store)
    assert cv.get_model() == store

def test_set_get_displayed_row():
    store = gtk.ListStore(str)
    store.append(['foo'])
    store.append(['boo'])
    cv = gtk.CellView()
    cv.set_model(store)
    cv.set_displayed_row((1,))
    assert cv.get_displayed_row() == (1,)
    cv.set_displayed_row((0,))
    assert cv.get_displayed_row() == (0,)
    cv.set_displayed_row(None)
    assert not cv.get_displayed_row()

@utils.pass_on_warnings
def test_set_displayed_row_oob():
    '''
    Ensure that a warning is printed when the displayed row is set to
    a tree path out of bounds.

    :bug: #539468
    '''
    store = gtk.ListStore(str)
    cv = gtk.CellView()
    cv.set_model(store)
    cv.set_displayed_row((1, 2, 3))
    assert not cv.get_displayed_row()

def test_get_size_of_row_oob():
    '''
    It is possible that ``get_size_of_row`` should emit a Warning
    instead.
    '''
    store = gtk.ListStore(str)
    cv = gtk.CellView()
    cv.set_model(store)
    req = cv.get_size_of_row((1, 2, 3))
    assert req.width == 0
    assert req.height == 0

@utils.pass_on_warnings    
def test_set_displayed_row_no_model():
    cv = gtk.CellView()
    cv.set_displayed_row((0, 1, 2))

def test_get_cell_renderers_changes_cell_renderers():
    '''
    Ensure that calling ``get_cell_renderers`` does not update the
    cell renderers in the ``gtk.CellView``.

    :bug: #539503
    '''
    store = gtk.ListStore(str)
    store.append(['foo'])
    cv = gtk.CellView()
    cv.set_model(store)
    cr = gtk.CellRendererText()
    cv.pack_start(cr)
    cv.add_attribute(cr, 'text', 0)
    cv.set_displayed_row((0,))
    text = cr.get_property('text')
    cv.get_cell_renderers()
    assert cr.get_property('text') == text

def test_cell_renderes_update_when_row_changes():
    '''
    Ensure that what the cell renderers show is updated when the row
    that is displayed changes in the ``gtk.CellView``.

    :bug: #539503
    '''
    store = gtk.ListStore(str)
    store.append(['foo'])
    cv = gtk.CellView()

    cr = gtk.CellRendererText()
    cv.pack_start(cr)
    cv.add_attribute(cr, 'text', 0)
    cv.set_model(store)
    cv.set_displayed_row((0,))

    assert cr.get_property('text') == 'foo'

def test_repeat_add_attributes():
    '''
    Ensure that when ``add_attribute`` is called repeatedly with the
    same attribute name, that the column for that attribute is
    updated.

    :bug: #528661
    '''
    store = gtk.ListStore(str, str)
    store.append(['one', 'two'])

    cr = gtk.CellRendererText()

    cv = gtk.CellView()
    cv.set_model(store)
    cv.set_displayed_row((0,))
    cv.pack_start(cr)
    cv.add_attribute(cr, 'text', 0)
    cv.add_attribute(cr, 'text', 1)

    # Hack to make the cell view update the renderers properties.
    cv.get_cell_renderers()
    assert cr.get_property('text') == 'two'

######################################################################
##### Rendering tests ################################################
######################################################################
def test_spacing_of_pack_start_renderers():
    '''
    Three text cell renderers are put in a cell view using pack
    start. When the view is rendered, each of the three cell renderers
    should occupy the same amount of space.
    '''
    cv = gtk.CellView()
    crs = [gtk.CellRendererText() for x in range(3)]
    for cr, col in zip(crs, ['#ff0000', '#00ff00', '#0000ff']):
        cr.set_property('background', col)
        cr.set_property('text', 'dummy text')
        cv.pack_start(cr)

    win = gtk.Window()
    win.add(cv)
    win.show_all()

    pixbuf = utils.widget_get_rendering(win)
    reds = utils.pixbuf_count_pixels(pixbuf, '#ff0000')
    greens = utils.pixbuf_count_pixels(pixbuf, '#00ff00')
    blues = utils.pixbuf_count_pixels(pixbuf, '#0000ff')
    assert reds == greens == blues

def test_cell_renderer_expanding():
    '''
    One cell renderer pack started in a cell view. It should expand to
    cover the whole widget.
    '''
    cv = gtk.CellView()

    cr = gtk.CellRendererText()
    cr.set_property('background', '#ff0000')
    cv.pack_start(cr)

    win = gtk.Window()
    win.add(cv)
    win.show_all()
    win.modify_bg(gtk.STATE_NORMAL, gdk.Color('#0000ff'))

    # Cell renderer should resize now to cover the whole widget.
    win.resize(500, 500)

    utils.gtk_process_all_pending_events()
    pixbuf = utils.widget_get_rendering(win)

    # Sometimes the resize doesn't seem to happen...
    assert pixbuf.get_width() == 500
    assert pixbuf.get_height() == 500
    assert utils.pixbuf_count_pixels(pixbuf, '#0000ff') == 0
