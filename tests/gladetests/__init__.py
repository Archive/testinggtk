'''
Tests for the ``gtk.glade`` package.
'''
import gtk
from gtk import glade

def test_new_widget_not_embedded_in_glade_interface():
    '''
    Tests what happens when you attempt to load a widget string that
    is not embedded in a <glade-interface> tag.

    Disabled due to segfault.

    :bug: #540217
    '''
    # xml = '''
    # <widget class="GtkComboBox" id="foo"></widget>
    # '''
    # glade.xml_new_from_buffer(xml, len(xml))

def test_load_combo_box():
    xml = '''
    <glade-interface>
        <widget class="GtkComboBox" id="foo"></widget>
    </glade-interface>        
    '''
    widget = glade.xml_new_from_buffer(xml, len(xml))
    assert isinstance(widget.get_widget('foo'), gtk.ComboBox)
