'''
Tests for the ``gtk.Toolbar`` class.
'''
import gtk
import utils

def test_style_property():
    toolbar = gtk.Toolbar()

    for style in (gtk.TOOLBAR_BOTH,
                  gtk.TOOLBAR_BOTH_HORIZ,
                  gtk.TOOLBAR_ICONS,
                  gtk.TOOLBAR_TEXT):
        toolbar.set_style(style)
        assert toolbar.get_style() == style

@utils.pass_on_warnings        
def test_mixing_button_and_toolbutton():
    '''
    Ensure that a warning is printed when attempting to mix gtk.Button
    and gtk.ToolButton in a gtk.Toolbar.
    '''
    toolbar = gtk.Toolbar()
    toolbar.add(gtk.ToolButton(gtk.STOCK_CLOSE))
    toolbar.add(gtk.Button('hi'))

def test_label_packing_in_toolbar_both():
    '''
    Ensure that a label inside a gtk.ToolButton is packed correctly
    when the button doesn't have an image and the toolbars style is
    ``gtk.TOOLBAR_BOTH``.

    The label should probably be aligned with the labels of the
    buttons that have an icon, using ``gtk.PACK_END``.

    :bug: #524222.
    '''
    toolbar = gtk.Toolbar()
    tb = gtk.ToolButton(label = 'moo')
    toolbar.add(tb)
    toolbar.set_style(gtk.TOOLBAR_BOTH)

    vbox = tb.get_child().get_child()
    label = vbox.get_children()[0]
    expand, fill, padding, pack_type = vbox.query_child_packing(label)
    assert pack_type == gtk.PACK_END
