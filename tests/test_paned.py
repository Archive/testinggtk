'''
Tests for the gtk.HPaned and gtk.VPaned classes.
'''
import gtk

DIVIDER_SIZE = 6

def show_widget_in_window(widget, width, height):
    win = gtk.Window()
    win.set_default_size(width, height)
    win.add(widget)
    win.show_all()

def test_default_allocates_equal_size():
    '''
    Ensure that by default, both childs in a gtk.HPaned are allocated
    the same width.
    '''
    widths = [400, 200, 300, 950]
    for width in widths:
        hpaned = gtk.HPaned()
        l1 = gtk.Label('l1')
        hpaned.pack1(l1, False, False)
        l2 = gtk.Label('l2')
        hpaned.pack2(l2, False, False)
        show_widget_in_window(hpaned, width, 200)

        l1width = l1.allocation.width
        l2width = l2.allocation.width

        center = width / 2
        lo_bound = center - DIVIDER_SIZE
        hi_bound = center + DIVIDER_SIZE
        assert lo_bound < l1width < hi_bound
        assert lo_bound < l2width < hi_bound

def test_default_divider_position():
    '''
    Ensure that the default divider position is 0.
    '''
    assert gtk.HPaned().get_position() == 0

def test_allocated_default_divider_position():
    '''
    Ensure that the divider position is in the center of the
    gtk.HPaned widget.
    '''
    hpaned = gtk.HPaned()
    l1 = gtk.Label('l1')
    hpaned.pack1(l1, False, False)
    l2 = gtk.Label('l2')
    hpaned.pack2(l2, False, False)

    show_widget_in_window(hpaned, 200, 200)
    assert 100 - DIVIDER_SIZE < hpaned.get_position() < 100 + DIVIDER_SIZE

def test_move_divider():
    '''
    Ensure that changing the divider position resizes the child widgets.
    '''
    hpaned = gtk.HPaned()
    l1 = gtk.Label('l1')
    hpaned.pack1(l1, False, False)
    l2 = gtk.Label('l2')
    hpaned.pack2(l2, False, False)

    
    
