'''
Tests for the ``gtk.SeparatorMenuItem`` class.
'''
import gobject
import gtk

def test_child_type():
    separator = gtk.SeparatorMenuItem()
    assert separator.child_type() == gobject.TYPE_NONE

def test_activate():
    '''
    Ensure that a ``gtk.SeparatorMenuItem`` cannot be activated.
    '''
    separator = gtk.SeparatorMenuItem()
    assert not separator.activate()
