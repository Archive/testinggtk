# -*- coding: utf-8 -*-
'''
This is TestingGTK a test suite for GTK+.

Hacking
=======
This project is hosted on GNOME SVN and everyone with an account is
encouraged to write test cases.

Style Guideline
---------------
Each test suite is named after the widget it tests, test_button.py
tests gtk.Button, test_label.py tests gtk.Label and so on.

Some tests crashes GTK+ which causes problems because nosetests cannot
recover from that so they are commented out and marked as disabled in
the code. It is probably a good idea to add a comment to the docstring
mentioning that they crash.

Many tests are connected to bug reports. Some bug reports contain
important information why GTK+ works in a certain way, or the test way
may contain the way to reproduce the bug. Such tests are marked using
``:bug: #123456`` which allows epydoc to add hyperlinks directly to
the reports.

Test cases are named "test" followed by a short description of what
the test tests. For example, the test
`test_action.test_set_bad_tool_item_type` tests what happens when you
set an invalid tool item type on a ``gtk.Action``.

There are some test names that are special:

test_default_attributes
  Almost all test modules have this test case. It tests that all
  object attributes have the correct default values after
  instantiation.

test_set_get_$someattr
  Exercises setting and getting the attribute $someattr.

test_wrapper_complete
  Ensures that a module, class or instance has all the required
  attributes.

Standard rules for writing unit test applies. Each test should be
self-contained, should not depend on any other test being run and
should clean up after itself. 


Release history
===============
Major changes between different versions of TestingGTK:

Major changes in 1.0.0
----------------------
Released at 20080609, first release containing 160 test cases.

:author: `Björn Lindqvist <bjourne@gmail.com>`__
:requires: Python 2.4+, GTK+ 2.12+, PyGTK 2.12+, nose 0.9.2+
:version: 1.0.0
:license: LGPL
:copyright: |copy| 2008 Björn Lindqvist

.. |copy| unicode:: 0xA9 .. copyright sign
'''
import gtk

def test_list_icon_size_enum():
    assert hasattr(gtk, 'ICON_SIZE_INVALID')
    assert hasattr(gtk, 'ICON_SIZE_MENU')
    assert hasattr(gtk, 'ICON_SIZE_SMALL_TOOLBAR')
    assert hasattr(gtk, 'ICON_SIZE_LARGE_TOOLBAR')
    assert hasattr(gtk, 'ICON_SIZE_BUTTON')
    assert hasattr(gtk, 'ICON_SIZE_DND')
    assert hasattr(gtk, 'ICON_SIZE_DIALOG')

def test_list_orientation_enum():
    assert hasattr(gtk, 'ORIENTATION_HORIZONTAL')
    assert hasattr(gtk, 'ORIENTATION_VERTICAL')

def test_list_selection_enum():
    assert hasattr(gtk, 'SELECTION_NONE')
    assert hasattr(gtk, 'SELECTION_SINGLE')
    assert hasattr(gtk, 'SELECTION_BROWSE')
    assert hasattr(gtk, 'SELECTION_MULTIPLE')
    assert hasattr(gtk, 'SELECTION_EXTENDED')

def test_list_ui_manager_toplevels():
    assert hasattr(gtk, 'UI_MANAGER_MENUBAR')
    assert hasattr(gtk, 'UI_MANAGER_TOOLBAR')
    assert hasattr(gtk, 'UI_MANAGER_POPUP')

def test_list_widget_flags():
    assert hasattr(gtk, 'TOPLEVEL')
    assert hasattr(gtk, 'NO_WINDOW')
    assert hasattr(gtk, 'REALIZED')
    assert hasattr(gtk, 'MAPPED')
    assert hasattr(gtk, 'VISIBLE')
    assert hasattr(gtk, 'SENSITIVE')
    assert hasattr(gtk, 'PARENT_SENSITIVE')
    assert hasattr(gtk, 'CAN_FOCUS')
    assert hasattr(gtk, 'HAS_FOCUS')
    assert hasattr(gtk, 'CAN_DEFAULT')
    assert hasattr(gtk, 'HAS_DEFAULT')
    assert hasattr(gtk, 'HAS_GRAB')
    assert hasattr(gtk, 'RC_STYLE')
    assert hasattr(gtk, 'COMPOSITE_CHILD')
    assert hasattr(gtk, 'NO_REPARENT')
    assert hasattr(gtk, 'APP_PAINTABLE')
    assert hasattr(gtk, 'RECEIVES_DEFAULT')
    assert hasattr(gtk, 'DOUBLE_BUFFERED')

def test_list_target_flags():
    assert hasattr(gtk, 'TARGET_SAME_APP')
    assert hasattr(gtk, 'TARGET_SAME_WIDGET')
    assert hasattr(gtk, 'TARGET_OTHER_APP')
    assert hasattr(gtk, 'TARGET_OTHER_WIDGET')

def test_list_direction_enum():
    assert hasattr(gtk, 'DIR_TAB_FORWARD')
    assert hasattr(gtk, 'DIR_TAB_BACKWARD')
    assert hasattr(gtk, 'DIR_UP')
    assert hasattr(gtk, 'DIR_DOWN')
    assert hasattr(gtk, 'DIR_LEFT')
    assert hasattr(gtk, 'DIR_RIGHT')

def test_grab_get_current():
    '''
    Ensure that ``gtk.grab_get_current`` returns ``None`` when no
    widget has the grab.
    '''
    assert not gtk.grab_get_current()
