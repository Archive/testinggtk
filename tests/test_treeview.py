import gtk
from gtk import gdk
import utils

def test_wrapper_complete():
    '''
    Ensure that all public attributes are wrapped.

    :bug: #530145
    '''
    assert hasattr(gtk.TreeView, 'get_tooltip_context')

def test_default_attributes():
    view = gtk.TreeView()

    assert view.get_enable_search()
    assert view.get_headers_clickable()
    assert view.get_headers_visible()
    assert view.get_show_expanders()

    assert not view.get_bin_window()
    assert not view.get_columns()
    assert not view.get_drag_dest_row()
    assert not view.get_enable_tree_lines()
    assert not view.get_expander_column()
    assert not view.get_fixed_height_mode()
    assert not view.get_grid_lines()
    assert not view.get_hover_expand()
    assert not view.get_hover_selection()
    assert not view.get_model()
    assert not view.get_reorderable()
    assert not view.get_rubber_banding()
    assert not view.get_rules_hint()
    assert not view.get_search_entry()
    assert not view.get_visible_range()
    assert not view.is_rubber_banding_active()

    assert view.get_level_indentation() == 0
    assert view.get_tooltip_column() == -1
    assert view.get_search_column() == -1

    assert view.get_cursor() == (None, None)

    rect = view.get_visible_rect()
    assert rect.x == 0
    assert rect.y == 0
    assert rect.width == 1
    assert rect.height == 0

    assert isinstance(view.get_hadjustment(), gtk.Adjustment)
    assert isinstance(view.get_vadjustment(), gtk.Adjustment)
    assert isinstance(view.get_selection(), gtk.TreeSelection)

def test_default_selection():
    view = gtk.TreeView()
    sel = view.get_selection()
    assert sel.get_selected() == (None, None)
    assert sel.get_mode() == gtk.SELECTION_SINGLE
    assert sel.get_tree_view() == view
    assert sel.get_selected_rows() == (None, [])
    assert sel.count_selected_rows() == 0

def test_get_bin_window():
    view = gtk.TreeView()
    assert not view.get_bin_window()

def test_create_row_drag_icon():
    view = gtk.TreeView()
    assert not view.create_row_drag_icon((0,))

def test_create_row_drag_icon_on_invalid_path():
    '''
    Ensure that a ``TypeError`` is raised if ``create_row_drag_icon``
    is called with ``None`` as the ``path``.
    '''
    view = gtk.TreeView()
    try:
        view.create_row_drag_icon(None)
        assert False
    except TypeError:
        assert True

@utils.fail_on_warnings    
def test_get_dest_row_at_pos():
    '''
    Ensure that getting the destination row at position (0,0) from a
    newly constructed ``gtk.TreeView`` works as expected.

    :bug: #539377
    '''
    view = gtk.TreeView()
    assert not view.get_dest_row_at_pos(0, 0)

@utils.fail_on_warnings 
def test_get_tooltip_context():
    '''
    Ensure that getting the tooltip context from a newly created
    ``gtk.TreeView`` works as expected.

    :bug: #539377
    '''
    view = gtk.TreeView()
    assert not view.get_tooltip_context(0, 0, False)

def test_get_tooltip_context_realized():
    '''
    Ensure that getting the tooltip context from a realized
    ``gtk.TreeView`` without a model works as expected.
    '''
    view = gtk.TreeView()
    win = gtk.Window()
    win.add(view)
    win.show_all()
    assert not view.get_tooltip_context(0, 0, False)
    assert not view.get_tooltip_context(0, 0, True)

def test_get_tooltip_context_on_populated_model_kbd_tip():
    '''
    Construct a realized ``gtk.TreeView`` with a model. Get the
    tooltip context at position (0,0) using a keyboard tooltip.

    ``get_tooltip_context`` should return a 3-tuple containing the
    views model, the path and a valid iterator in that model.
    '''
    store = gtk.ListStore(int)
    for x in range(10):
        store.append([1])
    view = gtk.TreeView(store)
    column = gtk.TreeViewColumn("c1", gtk.CellRendererText(), text = 0)
    view.append_column(column)
    win = gtk.Window()
    win.add(view)
    win.show_all()
    model, path, iter = view.get_tooltip_context(0, 0, True)

    assert model == store
    assert path == (0,)
    assert model.get_value(iter, 0) == 1

@utils.fail_on_warnings    
def test_get_path_at_pos():
    '''
    Ensure that getting the path at position (0,0) on a newly created
    ``gtk.TreeView`` works as expected.
    '''
    view = gtk.TreeView()
    assert not view.get_path_at_pos(0, 0)

def test_get_path_at_pos_on_populated_model():
    store = gtk.ListStore(int)
    for x in range(10):
        store.append([1])
    view = gtk.TreeView(store)
    col = gtk.TreeViewColumn("c1", gtk.CellRendererText(), text = 0)
    view.append_column(col)
    win = gtk.Window()
    win.add(view)
    win.show_all()

    path, column, x, y = view.get_path_at_pos(0, 0)
    assert path == (0,)
    assert column == col
    assert x == 0
    assert y == 0

def test_convert_widget_to_bin_window_coords():
    '''
    Test converting widget to bin_window coordinates on an unrealized
    ``gtk.TreeView``.
    '''
    view = gtk.TreeView()
    for wx, wy in (0, 0), (100, 100), (-10, 3), (99, -99):
        view.set_headers_visible(False)
        bx, by = view.convert_widget_to_bin_window_coords(wx, wy)
        assert wx == bx
        assert wy == by

        view.set_headers_visible(True)
        bx, by = view.convert_widget_to_bin_window_coords(wx, wy)
        assert wx == bx
        assert wy == by + 1

def test_none_adjustments():
    '''
    Ensure that setting the vertical and horizontal adjustments to
    ``None`` works.

    :bug: #529623
    '''
    view = gtk.TreeView()
    view.set_hadjustment(None)
    view.set_vadjustment(None)
    view.set_property('hadjustment', None)
    view.set_property('vadjustment', None)

def test_non_string_tooltip_column():
    '''
    Ensure that setting the tooltip column of a treeview to a
    non-integer column works as expected. This test used to segfault.

    :bug: #530146
    '''
    store = gtk.ListStore(int)
    store.append([3])
    view = gtk.TreeView(store)
    view.set_tooltip_column(0)

    win = gtk.Window()
    win.add(view)
    win.show_all()

    tt = gtk.Tooltip()
    view.emit('query-tooltip', 0, 0, True, tt)

@utils.pass_on_warnings
def test_scroll_to_cell_without_model():
    '''
    Calling ``scroll_to_cell`` on a ``gtk.TreeView`` without a model
    results in a GtkWarning.
    '''
    view = gtk.TreeView()
    view.scroll_to_cell((1, 2, 3))

@utils.pass_on_warnings
def test_expand_row_without_model():
    '''
    Calling ``expand_row`` on a ``gtk.TreeView`` without a model
    results in a GtkWarning.
    '''
    view = gtk.TreeView()
    view.expand_row((1, 2, 3), False)

@utils.pass_on_warnings
def test_collapse_row_without_model():
    '''
    Calling ``collapse_row`` on a ``gtk.TreeView`` without a model
    results in a GtkWarning.
    '''
    view = gtk.TreeView()
    view.collapse_row((1, 2, 3))

def test_selection_on_popuplated_treeview():
    '''
    If the treeview is populated and realized, then its selection
    should by default be the first row in its model.
    '''
    store = gtk.ListStore(str)
    for x in range(10):
        store.append(['hi'])
    view = gtk.TreeView(store)

    win = gtk.Window()
    win.add(view)
    win.show_all()

    sel = view.get_selection()
    assert sel.get_selected_rows() == (store, [(0,)])

######################################################################
##### Cursor tests ###################################################
######################################################################
@utils.pass_on_warnings    
def test_set_cursor_without_model():
    '''
    Ensure that calling ``set_cursor`` on a ``gtk.TreeView`` without a
    model works as expected. A warning should be shown and
    ``get_cursor`` should return ``(None, None)``.
    '''
    view = gtk.TreeView()
    view.set_cursor((0, 1, 2, 4, 5))
    assert view.get_cursor() == (None, None)

@utils.pass_on_warnings
def test_set_cursor_on_cell_without_model():
    '''
    Calling ``set_cursor_on_cell`` on a ``gtk.TreeView`` without a
    model results in a GtkWarnings.
    '''
    view = gtk.TreeView()
    view.set_cursor_on_cell((1, 2, 3), start_editing = True)

def test_set_get_cursor():
    '''
    Exercise setting and getting the cursor position.
    '''
    store = gtk.ListStore(str)
    for x in range(10):
        store.append(['str'])
    view = gtk.TreeView(store)
    assert view.get_cursor() == (None, None)
    for x in range(10):
        view.set_cursor((x,))
        assert view.get_cursor() == ((x,), None)

def test_set_int_cursor():
    '''
    Ensure that an integer can be used as the tree path. This is for
    backwards compatibility.

    :bug: #350252
    '''
    store = gtk.ListStore(str)
    store.append(['str'])
    store.append(['str'])
    view = gtk.TreeView(store)
    view.set_cursor(1)
    cursor_path, focus_column = view.get_cursor()
    assert cursor_path == (1,)

@utils.fail_on_warnings    
def test_set_cursor_with_empty_model():
    '''
    Ensure that setting a cursor on a tree view with an empty model
    works as expected.

    :bug: #546005
    '''
    store = gtk.ListStore(str)
    view = gtk.TreeView(store)
    view.set_cursor((1,))

######################################################################
##### Drag-and-drop and reorderability tests #########################
######################################################################
def test_set_get_reorderable():
    view = gtk.TreeView()
    assert not view.get_reorderable()
    view.set_reorderable(True)
    assert view.get_reorderable()
    view.set_reorderable(False)
    assert not view.get_reorderable()

def test_reorderable_unset_by_drag_dest():
    '''
    Ensure that enabling the view to act as a drag destination unsets
    the ``reorderable`` property.

    :bug: #540379
    '''
    view = gtk.TreeView()
    view.set_reorderable(True)
    view.enable_model_drag_dest([], gdk.ACTION_DEFAULT)
    assert not view.get_reorderable()

def test_reorderable_unset_by_drag_source():
    '''
    Ensure that enabling the view to act as a drag source unsets the
    ``reorderable`` property.

    :bug: #540379
    '''
    view = gtk.TreeView()
    view.set_reorderable(True)
    view.enable_model_drag_source(0, [], gdk.ACTION_DEFAULT)
    assert not view.get_reorderable()

def test_reorderable_sets_up_drag_lists():
    '''
    Ensure that correct drag source and destination lists are set up
    for the treeview when enabling column reordering.
    '''
    view = gtk.TreeView()
    view.set_reorderable(True)

    target_lists = (view.drag_source_get_target_list(),
                    view.drag_dest_get_target_list())
    for targets in target_lists:
        assert len(targets) == 1
        drag_type, target_flags, app_id = targets[0]
        assert drag_type == 'GTK_TREE_MODEL_ROW'
        assert target_flags == gtk.TARGET_SAME_WIDGET
        assert app_id == 0
