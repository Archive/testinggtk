'''
Tests for the ``gtk.ComboBox`` class.
'''
import atk
import gtk
import utils

def test_default_attributes():
    combo = gtk.ComboBox()
    assert combo.get_wrap_width() == 0
    assert combo.get_active() == -1
    assert combo.get_row_span_column() == -1
    assert combo.get_column_span_column() == -1
    assert not combo.get_active_iter()
    assert not combo.get_model()
    assert not combo.get_title()
    assert isinstance(combo.get_popup_accessible(), atk.NoOpObject)
    assert combo.get_cells() == []

def test_ensure_style_properties():
    '''
    Ensure that all style properties defined in the ``gtk.ComboBox``
    class exist.
    '''
    combo = gtk.ComboBox()
    for prop in 'appears-as-list', 'arrow-size', 'shadow-type':
        combo.style_get_property(prop)

@utils.pass_on_warnings
def test_get_active_text_without_model():
    '''
    Ensure that a GtkWarning is printed when ``get_active_text`` is
    called on a ``gtk.ComboBox`` with no model.
    '''
    combo = gtk.ComboBox()
    assert not combo.get_active_text()

def test_new_text_constructor():
    '''
    Ensure that a ``gtk.ComboBox`` constructed using
    ``gtk.combo_box_new_text`` has the correct state.
    '''
    combo = gtk.combo_box_new_text()
    model = combo.get_model()
    assert model
    assert isinstance(model, gtk.ListStore)
    assert not combo.get_active_iter()
    assert combo.get_active() == -1

def test_changed_signal():
    '''
    Ensure that the ``changed`` signal is emitted when a new item is
    selected.
    '''
    emitted = [False]
    def changed_cb(combo):
        emitted[0] = True
    combo = gtk.combo_box_new_text()
    combo.connect('changed', changed_cb)
    for x in range(5):
        combo.append_text(str(x))
    combo.set_active(1)
    assert emitted[0]

def test_set_none_active_iter():
    '''
    Ensure that a ``TypeError`` is raised if ``None`` is set as the
    active iter.
    '''
    combo = gtk.ComboBox()
    try:
        combo.set_active_iter(None)
        assert False
    except TypeError:
        assert True

@utils.pass_on_warnings
def test_append_text_to_invalid_model():
    '''
    Ensure that appending text to a ``gtk.ComboBox`` with a model
    whose first row is not a string column, produces a warning.

    This test used to segfault.

    :bug: #538395
    '''
    model = gtk.ListStore(gtk.gdk.Pixbuf)
    combo = gtk.ComboBox(model)
    combo.append_text('hi')

@utils.pass_on_warnings    
def test_negative_wrap_width():
    '''
    Ensure that a GtkWarning is printed if a negative argument is
    passed to ``set_wrap_width``.
    '''
    combo = gtk.ComboBox()
    combo.set_wrap_width(-1234)

def test_children():
    '''
    Ensure that a newly constructed ``gtk.ComboBox`` has a
    ``gtk.CellView`` as its only child.
    '''
    combo = gtk.ComboBox()
    children = combo.get_children()
    assert len(children) == 1
    assert isinstance(children[0], gtk.CellView)

def test_set_active_changes_displayed_row_in_cell_view():
    '''
    Ensure that the displayed row attribute in the ``gtk.CellView`` in
    the ``gtk.ComboBox`` is updated when the active row is changed.
    '''
    combo = gtk.combo_box_new_text()
    for x in range(10):
        combo.append_text('%s' % x)
    cellview = combo.get_children()[0]

    assert not cellview.get_displayed_row()
    for active in range(10):
        combo.set_active(active)
        assert cellview.get_displayed_row() == (active,)
