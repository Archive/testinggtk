'''
Tests for the ``gtk.Container`` class.
'''
import gtk
import utils

@utils.pass_on_warnings
def test_remove_not_present_widget():
    container = gtk.HBox()
    widget = gtk.Button()
    container.remove(widget)

@utils.pass_on_warnings
def test_toolbar_remove_not_present_widget():
    '''
    Remove from a ``gtk.Toolbar`` is special cased.
    '''
    container = gtk.Toolbar()
    widget = gtk.Button()
    container.remove(widget)

@utils.fail_on_warnings
def test_unparent_ref_count():
    '''
    Ensure that the reference count for a widget inside a container is
    correct after it has been unparented. The container still holds a
    reference to the widget so the container must be the first of them
    to be freed.

    :bug: #551349
    '''
    widget = gtk.HBox()
    container = gtk.HBox()
    container.add(widget)
    widget.unparent()

def test_set_get_focus_child():
    container = gtk.HBox()
    assert not container.get_focus_child()
    widget = gtk.Label()
    container.set_focus_child(widget)
    assert container.get_focus_child() == widget
    container.set_focus_child(None)
    assert not container.get_focus_child()
