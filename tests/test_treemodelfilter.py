'''
Tests for the ``gtk.TreeModelFilter`` class.
'''
import gtk
import utils

def test_iterator_in_visible_func():
    '''
    Ensure that the visible function is called twice when a row is
    appended. First with an empty row due to the row-inserted signal
    and then once again triggered by row-changed.
    '''
    cell_values = []
    def visible_func(model, iter):
        value = model.get_value(iter, 0)
        cell_values.append(value)
        visible_func.n_calls += 1
        
    visible_func.n_calls = 0
    liststore = gtk.ListStore(str)
    filter = liststore.filter_new()
    filter.set_visible_func(visible_func)
    liststore.append(['one'])
    assert visible_func.n_calls == 2
    assert not cell_values[0]
    assert cell_values[1] == 'one'

@utils.fail_on_warnings
def test_filter_multilevel_store():
    '''
    Ensure that filtering out all rows from a multi-level
    ``gtk.TreeStore`` works as expected.

    :bug: #464173
    '''
    hidden = False
    def visible_func(model, iter):
        return not hidden
    store = gtk.TreeStore(str)
    iter = store.append(None, ['row'])
    store.append(iter, ['row'])

    filter = store.filter_new()
    filter.set_visible_func(visible_func)
    view = gtk.TreeView(filter)
    hidden = True
    filter.refilter()
