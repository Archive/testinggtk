'''
Tests for the ``gtk.BindingSet`` class and associated functions.
'''
import gobject
import gtk
from tests import utils

def test_bool_arg_binding():
    '''
    Ensure that a keybinding signal with a boolean argument is
    emitted.
    '''
    class Test(gtk.Button):
        __gsignals__ = {
            'sig' : (gobject.SIGNAL_ACTION |
                     gobject.SIGNAL_RUN_LAST, None, (bool,))
            }
        def __init__(self):
            super(gtk.Button, self).__init__()
        def do_sig(self, val):
            self.val = val
    
    test = Test()
    gtk.binding_entry_add_signal(Test, gtk.keysyms.r, 0, 'sig', bool, True)
    gtk.bindings_activate(test, gtk.keysyms.r, 0)
    assert test.val
    gtk.binding_entry_add_signal(Test, gtk.keysyms.k, 0, 'sig', bool, False)
    gtk.bindings_activate(test, gtk.keysyms.k, 0)
    assert not test.val

@utils.pass_on_warnings
def test_keybinding_signal_on_gobject():
    '''
    Attempt to bind a keybinding to a signal in a non-gtk class.

    Test should fail with a warning message because only gtk classes
    can have keybindings.
    '''
    class Test47(gobject.GObject):
        pass
    gobject.type_register(Test47)

    # Add a signal to the class
    gobject.signal_new('asignal',
                       Test47,
                       gobject.SIGNAL_RUN_LAST | gobject.SIGNAL_ACTION,
                       gobject.TYPE_NONE,
                       (gobject.TYPE_INT,))
    gtk.binding_entry_add_signal(Test47, gtk.keysyms.x, 0, 'asignal', int, 5)
