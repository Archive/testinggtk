'''
Tests for the ``gtk.ScaleButton`` class.
'''
import gobject
import gtk
from gtk import gdk
import utils

def test_default_attributes():
    sb = gtk.ScaleButton(gtk.ICON_SIZE_INVALID, 0, 100, 2)
    assert isinstance(sb.get_adjustment(), gtk.Adjustment)
    assert sb.get_value() == 0
    assert sb.get_property('icons') == []
    assert sb.get_property('size') == gtk.ICON_SIZE_INVALID
    assert sb.get_property('value') == 0

def test_orientation_property():
    '''
    Ensure that ``gtk.ScaleButton`` has an ``orientation`` property
    and that it is set to ``gtk.ORIENTATION_VERTICAL`` by default.

    :bug: #442042
    '''
    sb = gtk.ScaleButton(gtk.ICON_SIZE_INVALID, 0, 100, 2)
    assert sb.get_property('orientation') == gtk.ORIENTATION_VERTICAL

def test_set_orientation_property():
    '''
    Ensure that ``gtk.ScaleButton``:s ``orientation`` property can be
    modified.

    :bug: #442042
    '''
    sb = gtk.ScaleButton(gtk.ICON_SIZE_INVALID, 0, 100, 2)
    for orientation in (gtk.ORIENTATION_HORIZONTAL,
                        gtk.ORIENTATION_VERTICAL):
        sb.set_property('orientation', orientation)
        assert sb.get_property('orientation') == orientation

def test_set_get_orientation():
    sb = gtk.ScaleButton(gtk.ICON_SIZE_INVALID, 0, 100, 2)
    for orient in (gtk.ORIENTATION_HORIZONTAL,
                   gtk.ORIENTATION_VERTICAL):
        sb.set_orientation(orient)
        assert sb.get_orientation() == orient

def test_horizontal_orientation():
    '''
    Ensure that the ``orientation`` property can be set at
    construction time.
    '''
    sb = gobject.new(gtk.ScaleButton,
                     orientation = gtk.ORIENTATION_HORIZONTAL)
    assert sb.get_property('orientation') == gtk.ORIENTATION_HORIZONTAL

def test_button_press_triggers_grab():
    '''
    Ensure that ``gtk.ScaleButton`` performs a grab when it received a
    button press event.
    '''
    sb = gtk.ScaleButton(gtk.ICON_SIZE_INVALID, 0, 100, 2)

    # Assign it a suitable gdk.Window
    sb.window = gdk.Window(None,
                           100, 100,
                           gdk.WINDOW_TOPLEVEL,
                           0,
                           gdk.INPUT_OUTPUT,
                           x = 120, y = 80)

    ev = gdk.Event(gdk.BUTTON_PRESS)
    sb.do_button_press_event(sb, ev)
    assert gtk.grab_get_current()

def test_popup_window_hierarchy():
    '''
    Ensure that the widget hierarchy in the popup window is correct.
    '''
    sb = gtk.ScaleButton(gtk.ICON_SIZE_INVALID, 0, 10, 2)

    # Assign it a suitable gdk.Window
    sb.window = gdk.Window(None,
                           100, 100,
                           gdk.WINDOW_TOPLEVEL,
                           0,
                           gdk.INPUT_OUTPUT,
                           x = 120, y = 80)

    ev = gdk.Event(gdk.BUTTON_PRESS)
    sb.do_button_press_event(sb, ev)

    scale = gtk.grab_get_current()
    window = scale.get_toplevel()
    assert isinstance(window, gtk.Window)

    frame = window.get_children()[0]
    assert isinstance(frame, gtk.Frame)

    vbox = frame.get_children()[0]
    assert isinstance(vbox, gtk.VBox)

    plus_button, vscale, minus_button = vbox.get_children()
    assert isinstance(plus_button, gtk.Button)
    assert plus_button.get_label() == '+'
    assert isinstance(vscale, gtk.VScale)
    assert isinstance(minus_button, gtk.Button)
    assert minus_button.get_label() == '-'

def test_popup_horizontal_orientation_window_property():
    '''
    Ensure that the widgets are packed in a ``gtk.HBox`` instead of a
    ``gtk.VBox``.

    :bug: #442042
    '''
    sb = gobject.new(gtk.ScaleButton,
                     orientation = gtk.ORIENTATION_HORIZONTAL)

    # Assign it a suitable gdk.Window
    sb.window = gdk.Window(None,
                           100, 100,
                           gdk.WINDOW_TOPLEVEL,
                           0,
                           gdk.INPUT_OUTPUT,
                           x = 120, y = 80)

    ev = gdk.Event(gdk.BUTTON_PRESS)
    sb.do_button_press_event(sb, ev)

    scale = gtk.grab_get_current()
    window = scale.get_toplevel()

    frame = window.get_children()[0]
    hbox = frame.get_children()[0]
    assert isinstance(hbox, gtk.HBox)

def test_name_of_popup_window():
    '''
    Ensure that the name of ``gtk.ScaleButton``:s popup window is
    "gtk-scalebutton-popup-window".

    :bug: #442042
    '''
    sb = gtk.ScaleButton(gtk.ICON_SIZE_INVALID, 0, 10, 2)

    # Assign it a suitable gdk.Window
    sb.window = gdk.Window(None,
                           100, 100,
                           gdk.WINDOW_TOPLEVEL,
                           0,
                           gdk.INPUT_OUTPUT,
                           x = 120, y = 80)

    ev = gdk.Event(gdk.BUTTON_PRESS)
    sb.do_button_press_event(sb, ev)

    scale = gtk.grab_get_current()
    window = scale.get_toplevel()
    print window.get_name()
    assert window.get_name() == 'gtk-scalebutton-popup-window'

@utils.pass_on_warnings
def test_unrealized_button_press():
    '''
    Ensure that a GtkWarning is printed when a ``gdk.BUTTON_PRESS``
    event is activated on an unrealized ``gtk.ScaleButton``.
    '''
    sb = gtk.ScaleButton(gtk.ICON_SIZE_INVALID, 0, 10, 2)
    ev = gdk.Event(gdk.BUTTON_PRESS)
    sb.event(ev)
