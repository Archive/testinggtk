'''
Tests for the ``gtk.Style`` class.
'''
import gtk

def test_default_style():
    '''
    Ensure that any widget has a style after it has been created.
    '''
    widget = gtk.Button()
    assert widget.get_style()

def test_set_none_style():
    '''
    Ensure that setting None as the style reverts it back to its
    original state.
    '''
    widget = gtk.Button()
    widget.set_style(None)
    assert widget.get_style()
