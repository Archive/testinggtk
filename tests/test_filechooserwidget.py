'''
Tests for the ``gtk.FileChooserWidget`` class.
'''
import gc
import gtk

def test_iterating_using_foreach_and_gc():
    '''
    Ensure that recursively visiting all widgets in a
    ``gtk.FileChooserWidget`` using ``foreach`` works as expected.

    :bug: #335766
    '''
    def walk(widget):
        gc.collect()
        if hasattr(widget, 'foreach'):
            widget.foreach(walk)
    fc = gtk.FileChooserWidget()
    walk(fc)
