'''
Tests for the ``gtk.Window`` class.
'''
import gtk
from gtk import gdk
import utils

def test_default_attributes():
    win = gtk.Window()
    assert win.get_opacity() == 1
    assert isinstance(win.get_group(), gtk.WindowGroup)

def test_default_flags():
    win = gtk.Window()
    flags = win.flags()
    assert gtk.TOPLEVEL & flags
    assert gtk.SENSITIVE & flags
    assert gtk.PARENT_SENSITIVE & flags
    assert gtk.DOUBLE_BUFFERED & flags

def test_default_allocation():
    '''
    The default allocation is (0,0)-[200,200].
    '''
    win = gtk.Window()
    win.show()
    assert win.allocation == gdk.Rectangle(0, 0, 200, 200)

def test_property_change_fires_window_state_event():
    '''
    Ensure that the 'window-state-event' signal is fired when the
    ``gtk.Window`` is maximized using `property_change`.
    '''
    emitted = [False]
    def state_event_cb(widget, ev):
        emitted[0] = bool(ev.changed_mask & gdk.WINDOW_STATE_MAXIMIZED)

    win = gtk.Window()
    win.connect('window-state-event', state_event_cb)
    win.realize()
    win.window.property_change(
        gdk.atom_intern("_NET_WM_STATE"),
        "ATOM",
        32,
        gdk.PROP_MODE_REPLACE,
        [gdk.atom_intern("_NET_WM_STATE_MAXIMIZED_HORZ"),
         gdk.atom_intern("_NET_WM_STATE_MAXIMIZED_VERT")])

    utils.gtk_process_all_pending_events()
    assert emitted[0]

def test_property_change_updates_window_state():
    '''
    Ensure that ``gdk.Window.get_state`` says that the window is
    maximized after it has been maximized using
    ``gdk.Window.property_change``.
    '''
    win = gtk.Window()
    win.realize()
    win.window.property_change(
        gdk.atom_intern("_NET_WM_STATE"),
        "ATOM",
        32,
        gdk.PROP_MODE_REPLACE,
        [gdk.atom_intern("_NET_WM_STATE_MAXIMIZED_HORZ"),
         gdk.atom_intern("_NET_WM_STATE_MAXIMIZED_VERT")])
    
    utils.gtk_process_all_pending_events()
    
    state = win.window.get_state()
    bool(state & gdk.WINDOW_STATE_MAXIMIZED)

def test_set_focus():
    '''
    Ensure that setting the focus to a widget in the ``gtk.Window``
    works.
    '''
    win = gtk.Window()
    button = gtk.Button()
    win.add(button)
    win.set_focus(button)
    assert win.get_focus() == button
    win.set_focus(None)
    assert not win.get_focus()

def test_list_toplevels():
    '''
    Ensure that all active ``gtk.Window`` widgets are listed in
    ``gtk.window_list_toplevels``.
    '''
    for toplevel in gtk.window_list_toplevels():
        toplevel.destroy()
    assert gtk.window_list_toplevels() == []

    windows = [gtk.Window() for x in range(3)]
    assert len(gtk.window_list_toplevels()) == 3
    for topwin, win in zip(gtk.window_list_toplevels(), windows):
        assert topwin == win

def test_grab_notify_emitted_when_shadowed():
    '''
    Ensure that ``grab-notify`` is emitted on a widget when
    ``grab_add`` is called on a widget that is not the ancestor of
    that widget.
    '''
    data = []
    def grab_notify_cb(widget, was_grabbed):
        data.append((widget, was_grabbed))

    # Two windows
    win = gtk.Window()
    win2 = gtk.Window()
    win.connect('grab-notify', grab_notify_cb)

    # Grab win2, win is now "shadowed" and grab-notify should be
    # emitted on it.
    win2.grab_add()

    assert data[0] == (win, False)

def test_set_get_resizable():
    win = gtk.Window()
    assert win.get_resizable()
    win.set_resizable(False)
    assert not win.get_resizable()
    win.set_resizable(True)
    assert win.get_resizable()

def test_resize_window():
    '''
    Test that resizing ``gtk.Window`` immidiately updates its
    size. Note that this only works because the window is not
    realized.
    '''
    win = gtk.Window()
    for size in (10, 20, 100, 95):
        win.resize(size, size)
        assert win.get_size() == (size, size)

def test_default_update_area():
    '''
    The update_area of the newly visible gdk.Window is
    (0,0)-[200,200]. Why 200?
    '''
    win = gtk.Window()
    win.show()
    area = win.window.get_update_area().get_clipbox()
    assert area == gdk.Rectangle(0, 0, 200, 200)

def test_resize_does_not_invalidate():
    '''
    Ensure that resizing a shown window does not invalidate it.

    :bug: #540310
    '''
    win = gtk.Window()
    win.show()
    gdk.window_process_all_updates()
    win.set_default_size(100, 100)
    win.resize(200, 200)
    assert not win.window.get_update_area()

@utils.swallow_warnings
def test_set_policy_does_not_invalidate():
    '''
    Ensure that setting the policy of a window does not invalidate it.

    :bug: #540310
    '''
    win = gtk.Window()
    win.show()
    gdk.window_process_all_updates()
    win.set_policy(False, False, False)
    assert not win.window.get_update_area()

def test_set_position_constraints_does_not_invalidate():
    '''
    Ensure that setting the window position constraint to
    ``gtk.WIN_POS_CENTER_ALWAYS`` does not invalidate it.

    :bug: #540310
    '''
    win = gtk.Window()
    win.show()
    gdk.window_process_all_updates()
    win.set_position(gtk.WIN_POS_CENTER_ALWAYS)
    assert not win.window.get_update_area()

def test_set_geometry_hints_does_not_invalidate():
    '''
    Ensure that setting the geometry hints for a window does not
    invalidate it.
    '''
    win = gtk.Window()
    win.show()
    gdk.window_process_all_updates()
    win.set_geometry_hints(win)
    assert not win.window.get_update_area()

def test_set_default_size_does_not_invalidate():
    '''
    Ensure that setting the default size of a ``gtk.Window`` does not
    invalidate it.

    :bug: #540310
    '''
    win = gtk.Window()
    win.show()
    gdk.window_process_all_updates()
    win.set_default_size(5321, 123)
    assert not win.window.get_update_area()

def test_set_gravity_does_not_invalidate():
    '''
    Ensure that setting the window gravity does not invalidate it.

    :bug: #540310
    '''
    win = gtk.Window()
    win.show()
    gdk.window_process_all_updates()
    win.set_gravity(gdk.GRAVITY_CENTER)
    assert not win.window.get_update_area()

def test_focus_widget_with_buttons():
    '''
    In a realized window containing three buttons, the first button
    added is the focus widget.
    '''
    buttons = [gtk.Button('b') for x in range(3)]

    hbox = gtk.HBox()
    for button in buttons:
        hbox.add(button)

    win = gtk.Window()
    win.add(hbox)
    win.show_all()
    assert win.get_focus() == buttons[0]

def test_focus_widget_with_treeviews():
    '''
    In a realized window containing three treeviews, the first
    treeview added is the focus widget.
    '''
    views = [gtk.TreeView() for x in range(3)]

    hbox = gtk.HBox()
    for view in views:
        hbox.add(view)

    win = gtk.Window()
    win.add(hbox)
    win.show_all()
    assert win.get_focus() == views[0]

def test_focus_widget_with_disabled_button():
    '''
    Two buttons are added to a window. The first button cannot be
    focused so the second button should be the focus widget.
    '''
    b1 = gtk.Button('b1')
    b1.set_property('can-focus', False)
    b2 = gtk.Button('b2')

    hbox = gtk.HBox()
    hbox.add(b1)
    hbox.add(b2)

    win = gtk.Window()
    win.add(hbox)
    win.show_all()
    assert win.get_focus() == b2

def test_focus_widget_with_disabled_treeview():
    '''
    Two treeviews are added to a window. The first treeview cannot be
    focused, so the second treeview should become the focus widget.

    :bug: #541391
    '''
    v1 = gtk.TreeView()
    v1.set_property('can-focus', False)
    v2 = gtk.TreeView()

    hbox = gtk.HBox()
    hbox.add(v1)
    hbox.add(v2)

    win = gtk.Window()
    win.add(hbox)
    win.show_all()
    assert win.get_focus() == v2

def test_keyboard_focus_of_unfocusable_widget():
    '''
    Ensure that an unfocusable widget cannot have the keyboard focus
    of a window.
    '''
    widget = gtk.Button()
    widget.set_property('can-focus', False)
    win = gtk.Window()
    win.add(widget)
    win.show_all()
    assert not win.get_focus()
    widget.grab_focus()
    assert not win.get_focus()

@utils.pass_on_warnings
def test_remove_nonexistent_accel_group():
    '''
    Ensure that a warning is shown when an accel group that does not
    exist in the windows list of accelerator groups is removed.
    '''
    ag = gtk.AccelGroup()
    win = gtk.Window()
    win.remove_accel_group(ag)

def test_destroy():
    '''
    :bug: #90213
    '''
    win = gtk.Window()
    win.destroy()

def test_destroy_removes_window_from_toplevels():
    '''
    Calling ``destroy()`` on a ``gtk.Window`` should remove it from
    the list of toplevels.
    '''
    for toplevel in gtk.window_list_toplevels():
        toplevel.destroy()
    windows = [gtk.Window() for x in range(2)]
    assert len(gtk.window_list_toplevels()) == 2
    windows[0].destroy()
    assert len(gtk.window_list_toplevels()) == 1
    windows[1].destroy()
    assert len(gtk.window_list_toplevels()) == 0

@utils.pass_on_warnings 
def test_set_parent_on_toplevel():
    '''
    Ensure that set_parent on a gtk.Window doesn't work because it is
    a top-level.
    '''
    win = gtk.Window()
    parent = gtk.HBox()
    win.set_parent(parent)
