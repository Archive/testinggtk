'''
Tests for the ``gtk.EventBox`` class.
'''
import gtk

def test_visible():
    eb = gtk.EventBox()
    assert not eb.get_property('visible')
    eb.show()
    assert eb.get_property('visible')
    eb.hide()
    assert not eb.get_property('visible')
