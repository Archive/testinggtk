'''
Tests for the gtk.SpinButton class.
'''
import gtk
from gtk import gdk
import utils

def test_n_children_of_parent():
    '''
    Ensure that the gtk.SpinButton widget does not incorrectly use its
    parent widgets window for gdk.Window's it creates.

    Widgets with their own windows should always use that window
    instead.

    :bug: #466000
    '''
    adj = gtk.Adjustment(0, 0, 0, 0, 0, 0)
    spin = gtk.SpinButton(adj, 1, 2)
    win = gtk.Window()
    win.add(spin)
    win.show_all()
    assert len(win.window.get_children()) == 1

def test_set_get_digits():
    adj = gtk.Adjustment(0, 0, 0, 0, 0, 0)
    spin = gtk.SpinButton(adj, 1, 2)
    assert spin.get_digits() == 2.0
    spin.set_digits(8)
    assert spin.get_digits() == 8
    spin.set_digits(0)
    assert spin.get_digits() == 0

def test_set_get_digits_property():
    adj = gtk.Adjustment(0, 0, 0, 0, 0, 0)
    spin = gtk.SpinButton(adj, 1, 2)
    assert spin.get_property('digits') == 2
    spin.set_property('digits', 0)
    assert spin.get_property('digits') == 0
    spin.set_property('digits', 10)
    assert spin.get_property('digits') == 10

@utils.pass_on_warnings
def test_set_negative_digits():
    '''
    Ensure that setting a negative number of digits results in a
    ``GtkWarning`` and leaves the digits attribute unchanged.

    :bug: #549124
    '''
    adj = gtk.Adjustment(0, 0, 0, 0, 0, 0)
    spin = gtk.SpinButton(adj, 1, 2)
    spin.set_digits(-10)
    assert spin.get_digits() == 2

@utils.pass_on_warnings
def test_configure_negative_digits():
    '''
    Ensure that using the ``configure`` method to set a negative
    number of digits does not work and leaves the digits attribute
    unchanged.

    :bug: #549124
    '''
    adj = gtk.Adjustment(0, 0, 0, 0, 0, 0)
    spin = gtk.SpinButton(adj, 1, 2)
    spin.configure(adj, 1, -10)
    assert spin.get_digits() == 2

def test_init_negative_digits():
    '''
    Ensure that a ``TypeError`` is raised when a ``gtk.SpinButton``
    with a negative number of digits is created. Probably should have
    been a ``ValueError`` but it is to late to change that now.
    '''
    adj = gtk.Adjustment(0, 0, 0, 0, 0, 0)
    try:
        spin = gtk.SpinButton(adj, 1, -10)
        assert False
    except TypeError:
        assert True

def test_value_changed_signal():
    '''
    Ensure that the ``value-changed`` signal is properly emitted on a
    ``gtk.SpinButton`` when its value is changed.
    '''
    emitted = [False]
    def value_changed_cb(spin):
        emitted[0] = True
    adj = gtk.Adjustment(0, 0, 100,
                         1, 10, 10)
    spin = gtk.SpinButton(adj, 1, 2)
    spin.connect('value-changed', value_changed_cb)
    adj.value = 33
    assert emitted[0]

def test_update_emits_input_signal():
    '''
    Ensure that calling ``update`` on a ``gtk.SpinButton`` emits the
    ``input`` signal.

    :bug: #464574
    '''
    # Disabled due to segfault.
    # emitted = [False]
    # def input_cb(spin, value_ptr):
    #    emitted[0] = True
    # spin = gtk.SpinButton(None, 1, 2)
    # spin.connect('input', input_cb)
    # spin.update()
    # assert emitted[0]

def test_output_signal():
    '''
    Ensure that the ``output`` signal is emitted when you change the
    value a ``gtk.SpinButton`` displays.
    '''
    emitted = [False]
    def output_cb(spin):
        emitted[0] = True

    adj = gtk.Adjustment(value = 1,
                         lower = 0,
                         upper = 100,
                         step_incr = 0.123,
                         page_incr = 10,
                         page_size = 10)        
    spin = gtk.SpinButton(adj, 1, 2)
    spin.connect('output', output_cb)
    spin.set_value(33)
    assert emitted[0]

def test_output_signal_none_adjustment():
    '''
    Ensure that the ``output`` signal is emitted when
    ``gtk.SpinButton`` is constructed using ``None`` as the
    ``gtk.Adjustment`` and the spin buttons value is changed.
    '''
    emitted = [False]
    def output_cb(spin):
        emitted[0] = True
    spin = gtk.SpinButton(None, 1, 2)
    spin.set_range(0, 100)
    spin.connect('output', output_cb)
    spin.set_value(33)
    assert emitted[0]

def test_value_changed_signal_on_high_prec_spin_button():
    '''
    Ensure that the ``value-changed`` signal is only emitted once when
    the value of a spin button with more decimals than are shown is
    changed.

    :bug: #316771
    '''
    n_emits = [0]
    def value_changed_cb(spin):
        n_emits[0] = n_emits[0] + 1
    adj = gtk.Adjustment(value = 1,
                         lower = 0,
                         upper = 100,
                         step_incr = 0.123,
                         page_incr = 10,
                         page_size = 10)
    spin = gtk.SpinButton(adj, 1, 2)
    spin.connect('value-changed', value_changed_cb)
    adj.value = 0.576
    spin.update()
    assert n_emits[0] == 1

def test_set_range_ignores_page_size():
    '''
    Ensure that the page_size of a ``gtk.Adjustment`` used in a
    ``gtk.SpinButton`` does not affect that spin buttons value when
    calling ``set_range``.

    :bug: #307963
    '''
    adj = gtk.Adjustment(1, 0, 100, 1, 10, 10)
    spin = gtk.SpinButton(adj)
    spin.set_value(95)
    spin.set_range(0, 100)
    assert spin.get_value() == 95

def test_button_up():
    '''
    Ensure that pressing the up button on a ``gtk.SpinButton``
    increments its value.
    '''
    adj = gtk.Adjustment(1, 0, 100, 1, 10, 10)
    spin = gtk.SpinButton(adj, 1, 2)
    spin.set_text('1')

    event = gdk.Event(gdk.BUTTON_PRESS)
    event.button = 1
    spin.do_button_press_event(spin, event)

    gtk.main_iteration(False)

    event = gdk.Event(gdk.BUTTON_RELEASE)
    event.button = 1
    spin.do_button_release_event(spin, event)

    assert spin.get_value() == 2.0

def test_key_up_commits():
    '''
    Ensure that pressing the up key commits the old value entered into
    the ``gtk.SpinButton`` before setting a new value.

    :bug: #106574
    '''
    adj = gtk.Adjustment(1, 0, 100, 1, 10, 10)
    spin = gtk.SpinButton(adj, 1, 2)
    spin.set_text('5')
    gtk.bindings_activate(spin, gtk.keysyms.Up, 0)
    assert spin.get_value() == 6

