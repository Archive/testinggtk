'''
Test for the deprecated ``gtk.Progress`` class.
'''
import gtk
import utils

def test_format_string_overflow():
    '''
    Ensure that it is not possible to cause an overflow using
    ``set_format_string``.

    :bug: #156844
    '''
    # Disabled due to segfault
    # pg = gtk.Progress()
    # pg.set_format_string('e' * 10000)
    # pg.get_current_text()

@utils.pass_on_warnings
def test_realizing_progress():
    '''
    Ensure that attempting to realize a ``gtk.Progress`` produces a
    warning.

    Disabled due to segfault

    :bug: #377699
    '''
    # pg = gtk.Progress()
    # win = gtk.Window()
    # win.add(pg)
    # pg.realize()
