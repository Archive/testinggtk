'''
Tests for the ``gtk.Button`` class.
'''
import gtk
from gtk import gdk
import utils

def test_default_flags():
    button = gtk.Button()
    flags = button.flags()
    assert flags & gtk.NO_WINDOW

def test_set_get_use_stock():
    '''
    Ensure that getting and setting the ``use_stock`` attribute works
    as expected.
    '''
    button = gtk.Button()
    assert not button.get_use_stock()
    button.set_use_stock(True)
    assert button.get_use_stock()
    button.set_use_stock(False)
    assert not button.get_use_stock()

@utils.pass_on_warnings
def test_warning_on_adding_child():
    '''
    Ensure that a warning is printed when an attempt is made to add a
    child to a button already containing a child.
    '''
    button = gtk.Button('moo')
    button.add(gtk.Label())

def test_set_button_image():
    '''
    Ensure that setting an image in a button creates a gtk.Alignment
    child to the button which contains a gtk.HBox which contains the
    gtk.Image.
    '''
    button = gtk.Button()
    button.set_image(gtk.Image())

    alignment = button.get_child()
    assert isinstance(alignment, gtk.Alignment)

    hbox = alignment.get_child()
    assert isinstance(hbox, gtk.HBox)

    children = hbox.get_children()
    assert len(children) == 1
    assert isinstance(children[0], gtk.Image)

def test_button_with_image_and_label():
    '''
    Ensure that a button with a label and and image contains a
    gtk.Alignment which contains a gtk.HBox which has two children,
    one gtk.Image and one gtk.Label.
    '''
    button = gtk.Button('hello')
    
    button.set_image(gtk.Image())

    alignment = button.get_child()
    assert isinstance(alignment, gtk.Alignment)

    hbox = alignment.get_child()
    assert isinstance(hbox, gtk.HBox)

    children = hbox.get_children()
    assert len(children) == 2
    assert isinstance(children[0], gtk.Image)
    assert isinstance(children[1], gtk.Label)

def test_underline_empty_constructor():
    '''
    Ensure that underline interpretation is on by default when the
    button is constructed using the empty constructor.

    :bug: #524187
    '''
    assert gtk.Button().get_use_underline()

def test_underline_text_constructor():
    '''
    Ensure that underline interpretation is on by default when the
    button is constructed using the string constructor.
    '''
    button = gtk.Button('hello')
    assert button.get_use_underline()

    button = gtk.Button('he_llo', use_underline = False)
    assert not button.get_use_underline()

def test_underline_underline_constructor():
    '''
    Ensure that underline interpretation is on when it is specified in
    the constructor and off when it is set to False in the
    constructor.

    :bug: #524187
    '''
    assert gtk.Button(use_underline = True).get_use_underline()
    assert not gtk.Button(use_underline = False).get_use_underline()

def test_underline_after_setting_text():
    '''
    Ensure that the underline property doesn't switch to true after
    setting the label text of a button.
    '''
    button = gtk.Button(use_underline = False)
    button.set_label('foobar')
    assert not button.get_use_underline()

def test_mnemonic_key_of_label():
    '''
    Ensure that the buttons label doesn't magically get a mnemonic key
    defined when use underline = False.

    :bug: #519429
    '''
    button = gtk.Button(use_underline = False)
    button.set_label('_a')
    button.set_image(gtk.Image())
    assert not button.get_use_underline()

    label = button.get_child().get_child().get_children()[1]
    assert label.get_mnemonic_keyval() == gtk.keysyms.VoidSymbol

def test_foreach():
    '''
    Ensure that a button has one non-internal child.
    '''
    times = [0]
    def foreach_cb(widget):
        times[0] += 1

    button = gtk.Button()
    button.set_image(gtk.Image())
    button.set_label("hi")
    button.foreach(foreach_cb)
    assert times[0] == 1

def test_button_looping():
    '''
    Disabled due to infinite loop.

    :bug: #539807
    '''
    # b1 = gtk.Button()
    # b2 = gtk.Button()
    # b3 = gtk.Button()
    # b1.add(b2)
    # b2.add(b3)
    # b3.add(b1)

def test_child_packing_in_image_button():
    '''
    Ensure that the packing settings of a buttons label and image
    widgets are correct.
    '''
    button = gtk.Button('text', gtk.STOCK_ADD)
    box = button.get_child().get_child()

    child1 = box.get_children()[0]
    expand, fill, padding, pack_type = box.query_child_packing(child1)
    assert expand == 0
    assert fill == 0
    assert padding == 0
    assert pack_type == gtk.PACK_START

    child2 = box.get_children()[1]
    expand, fill, padding, pack_type = box.query_child_packing(child2)
    assert expand == 0
    assert fill == 0
    assert padding == 0
    assert pack_type == gtk.PACK_END
