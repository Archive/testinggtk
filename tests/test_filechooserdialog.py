'''
Tests for the gtk.FileChooserDialog class.
'''
import gc
import gtk
import utils

def test_default_action():
    '''
    Ensure that the default action is
    ``gtk.FILE_CHOOSER_ACTION_OPEN``.

    :bug: #534042
    '''
    fc = gtk.FileChooserDialog()
    assert fc.get_action() == gtk.FILE_CHOOSER_ACTION_OPEN

def test_default_attributes():
    fc = gtk.FileChooserDialog()
    assert not fc.get_title()
    assert not fc.get_parent()
    assert not fc.get_has_separator()
    assert not fc.get_filename()

def test_construct_using_default_parameters():
    '''
    Ensure that the ``gtk.FileChooserDialog`` can be constructed using
    the documented default values specified.

    :bug: #309108
    '''
    fc = gtk.FileChooserDialog(title = None,
                               parent = None,
                               action = gtk.FILE_CHOOSER_ACTION_OPEN,
                               buttons = None,
                               backend = None)
    assert not fc.get_title()
    assert not fc.get_parent()
    assert not fc.get_has_separator()

def test_iterate_all_children():
    '''
    Ensure that recursively iterating over all children in a
    ``gtk.FileChooserDialog`` works as expected.
    '''
    def iterate_children(widget):
        if not isinstance(widget, gtk.Container):
            return
        for child in widget.get_children():
            iterate_children(child)
    fc = gtk.FileChooserDialog()
    iterate_children(fc)

def test_iterating_using_foreach_and_gc():
    '''
    Ensure that recursively visiting all widgets in a
    ``gtk.FileChooserDialog`` using ``foreach`` works as expected.
    '''
    fc = gtk.FileChooserDialog()
    def walk(widget):
        gc.collect()
        if hasattr(widget, 'foreach'):
            widget.foreach(walk)
    walk(fc)

def test_find_model_in_dialog():
    '''
    Ensure that getting the ``gtk.TreeModel`` of the ``gtk.TreeView``
    in the ``gtk.FilerChooserDialog`` works as expected.

    Disabled due to segfault.

    :bug: #538401
    '''
    #fc = gtk.FileChooserDialog()
    #path = ('VBox:FileChooserWidget:gtk.FileChooserDefault:'
    #        'VBox:HPaned:VBox:ScrolledWindow:TreeView')
    #view = utils.gtk_container_find_child(fc, path.split(':'))
    #assert view.get_model()

@utils.fail_on_warnings
def test_set_get_current_folder():
    '''
    :bug: #540235
    '''
    fc = gtk.FileChooserDialog()
    fc.set_current_folder('/')
    assert fc.get_current_folder() == '/'
    fc.set_current_folder('/does/probably/not/exist')
    assert fc.get_current_folder() == '/does/probably/not/exist'

