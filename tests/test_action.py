'''
Tests for the gtk.Action class.
'''
import gobject
import gtk

def test_none_stock_id():
    '''
    Ensure that a ``gtk.Action`` can be created with ``None`` as the
    stock_id.
    '''
    action = gtk.Action('name', 'action', 'tooltip', None)
    assert not action.get_property('stock-id')


def test_set_bad_tool_item_type():
    '''
    Ensure that TypeError is raised when ``set_tool_item_type`` is
    called with something that isn't a subclass of ``gtk.ToolItem``.

    :bug: #533644
    '''
    action = gtk.Action('name', 'action', 'tooltip', None)
    try:
        action.set_tool_item_type(None)
        assert False
    except TypeError:
        assert True

def test_get_label_of_button_proxy():
    '''
    A ``gtk.Button`` acting as the proxy for a ``gtk.Action`` should
    get that actions label.
    '''
    action = gtk.Action('name', 'action', 'tooltip', None)
    button = gtk.Button()
    action.connect_proxy(button)
    assert button.get_label() == 'action'

def test_get_label_of_use_stock_button_proxy():
    '''
    A ``gtk.Button`` with the ``use_stock`` attribute set to ``True``
    acting as the proxy for a ``gtk.Action`` should have the same
    label as the actions stock-id.
    '''
    action = gtk.Action('name', 'action', None, gtk.STOCK_ADD)
    button = gtk.Button()
    button.set_use_stock(True)
    action.connect_proxy(button)
    assert button.get_label() == 'gtk-add'

def test_set_tool_item_type():
    '''
    Ensure that setting the tool item type to be created from a
    ``gtk.Action`` works as expected.
    '''
    class Sub3(gtk.ToolButton):
        __gtype_name__ = 'Sub3'
        def __init__(self):
            super(Sub3, self).__init__()
    gobject.type_register(Sub3)
    
    action = gtk.Action('name', 'action', 'tooltip', None)
    action.set_tool_item_type(Sub3)
    item = action.create_tool_item()
    assert isinstance(item, Sub3)

def test_retrieve_proxy_menu_item_signal_in_tool_item_subclass():
    '''
    Ensure that a subclass of ``gtk.ToolButton`` assigned to a
    ``gtk.ToolItem`` using ``gtk.Action.set_tool_item_type`` receives
    the ``create_menu_proxy`` signal.

    :bug: #533623
    '''
    emitted = [False]
    class Sub2(gtk.ToolButton):
        __gtype_name__ = 'Sub2'
        def do_create_menu_proxy(self):
            emitted[0] = True
            
    gobject.type_register(Sub2)
    action = gtk.Action('name', 'action', 'tooltip', None)
    action.set_tool_item_type(Sub2)
    item = action.create_tool_item()
    item.retrieve_proxy_menu_item()
    assert emitted[0]

def test_set_get_visible():
    action = gtk.Action('name', 'action', 'tooltip', None)
    assert action.get_visible()
    for value in (False, True):
        action.set_visible(value)
        assert action.get_visible() == value

def test_set_get_sensitive():
    action = gtk.Action('name', 'action', 'tooltip', None)
    assert action.get_sensitive()
    for value in (False, True):
        action.set_sensitive(value)
        assert action.get_sensitive() == value

def test_set_get_visible_overflown():
    action = gtk.Action('name', 'action', 'tooltip', None)
    assert action.get_property('visible-overflown')
    for value in (False, True):
        action.set_property('visible-overflown', value)
        assert action.get_property('visible-overflown') == value

def test_menu_item_created_if_visible_overflown():
    '''
    A ``gtk.Action`` should create a ``gtk.MenuItem`` as the proxy
    menu item for a ``gtk.ToolItem`` with the menu item id
    "gtk-action-menu-item", if the visible-overflown property is
    ``True`` but not otherwise.
    '''
    action = gtk.Action('name', 'action', 'tooltip', None)
    item = action.create_tool_item()
    item.retrieve_proxy_menu_item()
    assert item.get_proxy_menu_item('gtk-action-menu-item')
    action.set_property('visible-overflown', False)
    item.retrieve_proxy_menu_item()
    assert not item.get_proxy_menu_item('gtk-action-menu-item')

def test_set_get_hide_if_empty():
    action = gtk.Action('name', 'action', 'tooltip', None)
    assert action.get_property('hide-if-empty')
    action.set_property('hide-if-empty', False)
    assert not action.get_property('hide-if-empty')
    action.set_property('hide-if-empty', True)
    assert action.get_property('hide-if-empty')

def test_notify_emitted_on_hide_if_empty():
    '''
    Ensure that a notify signal is emitted when the ``hide-if-empty``
    property is changed.
    '''
    changed_param = []
    def notify_cb(action, param):
        changed_param.append(param)
    action = gtk.Action('name', 'label', 'tooltip', None)
    action.connect('notify', notify_cb)
    action.set_property('hide-if-empty', False)
    assert changed_param[0].name == 'hide-if-empty'

def test_notify_emitted_on_visible_overflown():
    '''
    Ensure that a correct notify signal is emitted when the
    ``visible-overflown`` property is changed.
    '''
    changed_param = []
    def notify_cb(action, param):
        changed_param.append(param)
    action = gtk.Action('name', 'label', 'tooltip', None)
    action.connect('notify', notify_cb)
    action.set_property('visible-overflown', False)
    assert changed_param[0].name == 'visible-overflown'

def test_create_menu():
    '''
    The default ``gtk.Action`` class always return ``None`` for
    ``create_menu``.
    '''
    action = gtk.Action('name', 'label', 'tooltip', None)
    assert not action.create_menu()
